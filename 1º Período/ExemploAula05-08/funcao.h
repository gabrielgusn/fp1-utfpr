int quadrado(int num)
{
    return(num*num);
}

int primo(int num)
{
    int i, contDiv=0;

    for(i=1; i<=num; i++)
    {
        if(num%i==0)
        {
            contDiv++;
        }
    }
    return(contDiv);
}

char repete(char rep)
{
    do
        {
            printf("Deseja repetir?\n");
            setbuf(stdin, NULL);
            scanf("%c", &rep);
            if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
            {
                printf("Caractere Invalido\n");
            }
        }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
        return(rep);
}
