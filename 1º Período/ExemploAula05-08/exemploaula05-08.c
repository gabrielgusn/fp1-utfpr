#include <stdio.h>
#include "funcao.h"

int main(void)
{
    int num;
    char rep;
    do
    {
        printf("Insira um num: ");
        scanf("%d", &num);

        printf("Quadrado do numero: %d\n", quadrado(num));

        if(primo(num) == 2)
        {
            printf("%d eh primo\n", num);
        }
        else
        {
            printf("%d nao eh primo\n", num);
        }
        rep=repete(rep);
    }
    while(rep == 'S' || rep == 's');

    return 0;
}
