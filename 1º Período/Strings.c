#include <stdio.h>
#include <string.h>

/*int main()
{
    char nome[30];

    printf("Informe o nome: ");
    gets(nome);
    puts(nome);
    printf("String : %s\n", nome);

    char string[30] = {"Oitenta eh a melhor idade"};
    puts(string);
    int num;
    num=strlen(string);
    printf("Tamanho da string: %d\n", strlen(string));
    printf("for: \t");
    for(int i=0; i<=num; i++)
    {
        printf("%c", string[i]);
    }
}*/

#include <stdio.h>

int main(void)
{
    int i=0, j=0;
    char str1[30], str2[30];

    printf("Informe uma string: ");
    gets(str1);

    while(str1[i] != '\0')
    {
        i++;
    }

    printf("String1: %s\n", str1);
    printf("Valor de i: %d\n", i);

    i--;

    while(str1[i] >= 0)
    {
        str2[j] = str1[i];
        i--;
        j++;
    }

    str2[j] = '\0';

    printf("String2: %s\n", str2);

    return 0;
}
