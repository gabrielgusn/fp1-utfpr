/* nao consegui fazer uma estrutura que contasse corretamente a quantia de vezes que cada numero se repetia
por algum motivo, somente o ultimo numero conta certo as repeticoes*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{

    char rep;

    do
    {
        int vet[10], vetNaoRep[10], contIgual=0, vetRep[10], i=0, j=0, x=0, y=0, aux;
        int contNum=0;
        setbuf(stdin, NULL);//setbuf para nao dar problema na repeticao do programa

        srand(time(NULL));//inicializacao da rand

        for(i=0; i<10; i++)//declaracao dos valores do vetor
        {
            vet[i]=rand()%11;
        }

        for(i=9; i>0; i--)//estrutura de ordenacao do vetor
        {
            for(j=0; j<i; j++)
            {
                if(vet[j]>vet[j+1])
                {
                    aux = vet[j];//auxiliar para manter o valor do vetor na posicao j
                    vet[j] = vet[j+1];
                    vet[j+1] = aux;// atribuicao do vetor na posicao subsequente � posicao j do vetor pelo seu valor antigo(aux)
                }
            }
        }

        printf("=== VETOR ===\n");//estrutura para imprimir o vetor
        for(i=0; i<10; i++)
        {
            printf("%d\t", vet[i]);
        }

        for(i=0; i<10; i++)//estrutura do vetor com os valores que nao se repetem
        {
            contIgual=0;
            for(j=0; j<10; j++)
            {
                if(j != i && vet[i]== vet[j])// "j != i" para que ele nao fa�a a verifica��o do vetor com ele mesmo
                {                           // pois como ele compara uma posicao com todas, nunca encontraria um valor que nao se repetisse
                    contIgual++;//caso entre nesse condicional, significa que existe um numero igual no vetor
                }
            }
            if(contIgual==0)//se contIgual for 0, significa que nao entrou na condicional anterior, logo o valor nao se repete
            {
                vetNaoRep[y]=vet[i];// usei y=0 na declaracao de variaveis para utilizar aqui
                y++;
            }
        }

        printf("\n\n=== NAO REPETIDOS ===\n");
        for(i=0; i<y; i++)//imprime os valores que nao se repetem ate a posicao y do vetor(que � a quantia de valores)
        {                 //daria para fazer de forma que o vetor de nao repetidos tivesse o tamanho exato da quantia de nueros que nao se repetem, mas nao tive tempo para fazer.
            printf("%d\t", vetNaoRep[i]);
        }

        aux=-1;// -1 pois so tenho valores entre 0 e 10 para analisar

        for(i=0; i<10; i++)// estrutura do vetor de repeticao
        {
            for(j=0; j<10; j++)
            {
                if(j!= i && vet[i]==vet[j] && vet[j]!= aux)// "!= aux" para que nao haja repeticao de elementos no vetor
                {
                    vetRep[x]=vet[i];
                    aux=vetRep[x];
                    x++;//usei x=0 na declaracao de variaveis
                }
            }
        }

        printf("\n\n=== REPETIDOS ===\n ", x);
        for(i=0; i<x; i++)
        {
            printf("%d\t", vetRep[i]);
        }

        int matriz[x][10];//declarei a matriz aqui para que a quantia de linhas seja exatamente igual a quantia de repetidos

        for(i=0; i<x; i++)//estrutura da matriz
        {
            contNum=0;
            matriz[1][i]=vetRep[i];
            for(j=0; j<10; j++)
            {
                if(i!=j && vetRep[i]==vet[j])
                {
                    contNum++;//variavel que conta a quantia de vezes que o numero aparece
                }
            }
            matriz[i][2]=contNum;
        }

        printf("\n\n=== MATRIZ ===\n");
        printf("Valor\t Ocorrencias\n");
        for(i=0; i<x; i++)
        {
            {
            printf("%d\t%d", matriz[1][i], matriz[2][i]);
            printf("\n");
            }
        }

        printf("\nDeseja Repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
    }while(rep == 'S' || rep == 's');

    return 0;
}
