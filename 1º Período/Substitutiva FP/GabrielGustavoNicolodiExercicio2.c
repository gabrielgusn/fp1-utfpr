/* */

#include <stdio.h>
#include <stdlib.h>

int primo(int num);

int main()
{
    char str[50];
    int vet[50], i=0, j=0,k=0, contSpace=0, vetPrimo[50];

    printf("Informe uma string: ");//entrada da string
    gets(str);

    printf("\n=== STRING ===\n");
    puts(str);

    while(str[i] != '\0')// percorre a string
    {
        if(str[i]== ' ' && str[i+1] == ' ')//condicional para delimitar a quantia de espa�os subsequentes, se i+1 nao for espa�o, entao entra no proximo condicional
        {
            contSpace++;
        }
        else if(str[i] == ' ' && str[i+1] != ' ')//significa que � o ultimo espa�o, logo muda para a proxima posicao no vetor e incrementa 1 no contador de espaco
        {
            contSpace++;
            vet[j]=contSpace;
            j++;
            contSpace=0;// como � o ultimo espa�o subsequente, reinicio o contador
        }
        i++;
    }

    printf("\n=== VETOR ===\n");// estrutura para imprimir o vetor
    for(i=0; i<j; i++)// ate j para imprimir somente os valores que foram manipulados
    {
        printf("%d\t", vet[i]);
    }

    for(i=0; i<j; i++)
    {
        if(primo(vet[i])==0)//condicional com a funcao para conferir se eh um numero primo
        {
            vetPrimo[k]=vet[i];
            k++;//para declarar a posicao no vetor
        }
    }

    printf("\n=== PRIMOS ===\n");
    for(i=0; i<k; i++)//imprime de i ate k para imprimir somente as posicoes manipuladas no vetor
    {
        printf("%d\t", vetPrimo[i]);
    }

    printf("\n");

    return 0;
}

int primo(int num)//implementei essa fun��o para organizar melhor minha l�gica no exercicio
{
    int contDiv=0;

    for(int i=1; i<=num; i++)
    {
        if(num%i==0)
        {
            contDiv++;
        }
    }
    if(contDiv==2)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
