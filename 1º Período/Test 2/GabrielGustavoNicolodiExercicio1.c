#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

int main()
{
    int vet1[5], vet2[5], intersecao[5], uniao[10], num, verIgual;
    char rep;

    do
    {
        int contInt=0, contUni=10, i=0;

        srand(time(NULL));

        do//declaracao de valores primeiro vetor
        {
            vet1[i]=rand()%(15+1);
            verIgual=0;
            for(int j=0; j<i; j++)
            {
                if(vet1[i]==vet1[j])
                {
                    verIgual=1;
                }
            }
            if(verIgual==0)
            {
                i++;
            }
        }while(i<5);

        i=0;

        do//declaracao de valores segundo vetor
        {
            vet2[i]=rand()%(10+1);
            verIgual=0;
            for(int j=0; j<i; j++)
            {
                if(vet2[i]==vet2[j])
                {
                    verIgual=1;
                }
            }
            if(verIgual==0)
            {
                i++;
            }
        }while(i<5);

        i=0;

        for(int j=0; j<5; j++)
        {
            for(int k=0; k<5; k++)
            {
                if(vet1[j]==vet2[k])
                {
                    intersecao[i]=vet1[j];
                    i++;
                    contInt++;
                    break;
                }
            }
        }

        for(int j=0; j<10; j++)
        {
            for(int k=0; k<5; k++ )
            {
                uniao[k]=vet1[k];
                if(uniao[k]!=vet2[k])
                {
                    uniao[k+5]=vet2[k];
                }
                else
                {
                    contUni--;
                }
            }
        }

        printf("\n==== VETOR 1 ====\n");
        for(i=0; i<5; i++)
        {
            printf("%d\t", vet1[i]);
        }

        printf("\n==== VETOR 2 ====\n");
        for(i=0; i<5; i++)
        {
            printf("%d\t", vet2[i]);
        }

        printf("\n==== INTERSECAO ====\n");
        for(i=0; i<contInt; i++)
        {
            printf("%d\t", intersecao[i]);
        }
        printf("\n==== UNIAO ====\n");
        for(i=0; i<contUni; i++)
        {
            printf("%d\t", uniao[i]);
        }


        printf("\n");

        printf("\nDeseja Repetir? \n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        rep=toupper(rep);
    }while(rep=='S');

}
