#include <stdio.h>
#include <stdlib.h>

int main()
{
    int qtd, num=0, anterior=0, atual=0, maior=0, menor=0;
    do
    {
        printf("Digite o valor de n: ");
        scanf("%d", &qtd);
        if(qtd<1)
        {
            printf("O valor informado deve ser maior que 0\n");
        }
    }while(qtd<1);//finalizei a estrutura aqui para repetir somente a leitura de qtd caso qtd<1

    for(int i=1; i<=qtd; i++)//repete qtd vezes deve ser informado um valor
    {
        anterior=num;//posicionado antes do scanf para que seu valor seja declarado sempre antes da nova declara��o do num
        printf("Informe o valor %d: ", i);
        scanf("%d", &num);
        atual=num;
        if(menor==0 && maior==0)//para ser verdadeiro uma unica vez na execu��o do codigo
        {
            maior=num;
            menor=num;
        }
        if(anterior<atual)
        {
            maior=atual;
        }
        if(anterior>atual)
        {
            printf("Sequencia nao ordenada!\n\n");
            break;//significa que a ordem nao � crescente entao finaliza a estrutura com o break
        }
    }
    if(anterior<atual)
        {
            printf("Sequencia Ordenada!\n\n");
        }
    printf("Maior valor da sequencia: %d\n", maior);
    printf("Menor valor da sequencia: %d\n", menor);
    system("pause");
    return 0;
}
