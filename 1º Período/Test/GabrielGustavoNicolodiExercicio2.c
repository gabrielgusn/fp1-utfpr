#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    char opt;
    int contCar=0, contMin=0;
    do//valida as entradar de opt para serem diferentes de 0
    {
        int contDiv=0, contPrint=0, soma=0;
        printf("Informe um caractere: ");
        setbuf(stdin,NULL);
        scanf("%c", &opt);
        contCar++;/*respons�vel por contar quantos caracteres foram informados
                  como n�o � pra ser considerado o char "0", no final da porcentagem
                  fiz contCar-1 para reduzir esse valor*/
        if(opt>='1' && opt<= '9')
        {
            int ascii=(int)opt;//para converter o char no seu valor decimal ascii inteiro
            for(int i=ascii; i>0; i--)
            {
                if(i!=1)
                {
                    printf("%d *", i);
                }
                else if(i==1)
                {
                    printf("%d \n", i);
                }
            }
        }
        if((opt>='a' && opt<='z') || (opt>='A' && opt<='Z'))/* pensei em fazer opt=toupper(opt) mas nao fiz
                                                            pois seriam os mesmos valores ascii e nao daria certo o resultado*/
        {
            int ascii=(int)opt;
            for(int i=1; i<=ascii; i++)
            {
                contDiv=0;
                for(int j=1; j<=ascii; j++)//repeticao para testar todas as divisoes do numero
                {
                    if(i%j==0)
                    {
                        contDiv++;
                    }
                }
                if(contDiv==2)//condicional para numero primo
                {
                    printf("%d \t", i);
                    contPrint++;//sempre que ocorre um numero primo, ele soma+1 para o contPrint, que utilizo numa condicional abaixo
                    soma=soma+i;
                }
                if(contPrint==6)//sempre que contPrint chega a 6, significa que foram impressos 6 numeros, entao ira pular linha
                {
                    printf("\n");
                    contPrint=0;
                }
            }
            printf("\nSoma dos primos: %d\n", soma);
        }
        if(opt>='a' && opt<='z')// para contar quantas letras minusculas
        {
            contMin++;
        }
    }while(opt!='0');
    printf("\nPercentual de Minusculas: %.2f%%\n\n", (float)(100*contMin)/(contCar-1));//fiz uma regra de 3 para a porcentagem a fim de nao precisar declarar mais uma vari�vel

    system("pause");
    return 0;
}
