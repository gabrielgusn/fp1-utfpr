/* 3) Fazer uma fun��o que verifica se um n�mero � um quadrado perfeito. Um n�mero � quadrado perfeito se
possui como raiz quadrada um valor inteiro. A fun��o para obter a raiz quadrada � sqrt() e est� na biblioteca
math.h. Por exemplo, 25 � um quadrado perfeito porque raiz quadrada de 25 � 5. Utilizar essa fun��o para:
a) Verificar se um n�mero, informado pelo usu�rio, � um quadrado perfeito.
b) Mostrar quais n�meros de um intervalo s�o quadrados perfeitos.
Use menu de op��es (com switch-case) e implemente a repeti��o de programa. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

int palindromo(int num);
char repete(char rep);
void intervalo_palindromo();

int main()
{
    char option, rep;
    int num, limSup, limInf, digit=0, aux;

    do
    {
        setbuf(stdin,NULL);
        do
        {
            digit=0;
            printf("1 - Verifica se um numero eh um palindromo\n2 - Mostra os numeros palindromos que pertencem a um intervalo\n");
            printf("Opcao: ");
            setbuf(stdin,NULL);
            scanf("%c", &option);
            if(option != '1' && option != '2')
            {
                printf("Opcao Invalida!\n\n");
            }
        }while(option != '1' && option != '2');

        switch(option)
        {
        case '1':
            do
            {
                digit=0;
                printf("\nInforme um numero: ");
                scanf("%d", &num);
                aux=num;
                while(aux!=0)
                {
                    aux=aux/10;
                    digit++;
                }
                if(digit<2 || digit>10)
                {
                    printf("Quantia de digitos invalida\n");
                }
            }while(digit<2 || digit>10);

            if(palindromo(num)==1)
            {
                printf("\n%d eh palindromo\n", num);
            }
            else
            {
                printf("%\nd nao eh palindromo\n", num);
            }
            break;
        case '2':
            intervalo_palindromo();
            break;
        default:
            printf("Caractere Invalido\n\n");
        }
        setbuf(stdin,NULL);
        rep=repete(rep);
    }while(rep=='S' || rep == 's');

    return 0;
}

int palindromo(int num)
{
    int hold, inverso=0, result=0;

    hold = num;

    while(hold!=0)
    {
        inverso = (inverso * 10) + hold % 10;
        hold = hold/10;
    }
    if (num == inverso)
    {
        result = 1;
    }

    return(result);
}

void intervalo_palindromo()
{
    int limInf, limSup, digit=0, aux;

    do
    {
        do
        {
            digit=0;
            printf("\nInforme o limite inferior: ");
            scanf("%d", &limInf);
            aux=limInf;
            while(aux!=0)
            {
                aux=aux/10;
                digit++;
            }
            if(digit<2 || digit>10)
            {
                printf("\nQuantia de digitos invalida\n");
            }
        }while(digit<2 || digit>10);

        do
        {
            digit=0;
            printf("\nInforme o limite superior: ");
            scanf("%d", &limSup);
            aux=limSup;
            while(aux!=0)
            {
                aux=aux/10;
                digit++;
            }
            if(digit<2 || digit>10)
            {
                printf("\nQuantia de digitos invalida\n");
            }
        }while(digit<2 || digit>10);
        if(limInf>=limSup)
        {
            printf("\nLimite Inferior nao pode ser maior que o Superior\n\n");
        }
    }while(limInf>=limSup);
    printf("\n");

    for(int num=limInf; num<=limSup; num++)
    {
        if(palindromo(num)==1)
        {
            printf("%d\t", num);
        }
    }
    printf("\n");
}


char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');

    return(rep);
}
