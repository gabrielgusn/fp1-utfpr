/* 2) Fazer uma fun��o que verifica se um n�mero � retangular. Um n�mero � retangular se ele pode ser obtido
a partir da soma de uma sequ�ncia de n�meros pares, iniciando em 2. Por exemplo: 30 � retangular porque
resulta da soma de 2 + 4 + 6 + 8 + 10. Utilizar essa fun��o para:
a) Verificar se um n�mero, informado pelo usu�rio, � retangular.
b) Mostrar quais n�meros de um intervalo s�o retangulares.
Use menu de op��es (com switch-case) e implemente a repeti��o de programa */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int triangular(int num);
char repete(char rep);
void intervalo_retangulo();

int main()
{
    char option, rep;
    int num, limSup, limInf;

    do
    {
        setbuf(stdin,NULL);
        do
        {
            printf("1 - Verifica se um numero eh retangular\n2 - Mostra os numeros retangulares que pertencem a um intervalo\n");
            printf("Opcao: ");
            setbuf(stdin,NULL);
            scanf("%c", &option);
            if(option != '1' && option != '2')
            {
                printf("Opcao Invalida!\n\n");
            }
        }while(option != '1' && option != '2');

        switch(option)
        {
        case '1':
            printf("Informe um numero: ");
            scanf("%d", &num);
            retangular(num);
            break;
        case '2':
            intervalo_retangulo();
            break;
        default:
            printf("Caractere Invalido\n\n");
        }
        setbuf(stdin,NULL);
        rep=repete(rep);
    }while(rep=='S' || rep == 's');

    return 0;
}

int retangular(int num)
{
    int retangulo=0, result=0;
    printf("\n");
    for(int i=2; i<=num; i++)
    {
        if(i%2==0)
        {
            retangulo = retangulo + i;
            if(retangulo==num)
            {
                printf("%d eh retangular ==>", num);
                for(int j=2; j<=i; j=j+2)
                {
                    if(j!=i)
                    {
                        printf("% d +", j);
                    }
                    else
                    {
                        printf(" %d = %d\n", j, num);
                    }
                }
                return 1;
                break;
            }
        }
    }
    printf("%d nao eh um numero retangular\n", num);
}

void intervalo_retangulo()
{
    int num, limInf, limSup;

    do
    {
        printf("Informe o limite inferior: ");
        scanf("%d", &limInf);
        printf("Informe o limite superior: ");
        scanf("%d", &limSup);
        if(limInf>=limSup)
        {
            printf("Limite Inferior nao pode ser maior que o Superior\n\n");
        }
    }while(limInf>=limSup);
    printf("\n");
    int result=0;

    for(int num=limInf; num<=limSup; num++)
    {
        int retangulo=0;
        for(int i=2; i<=num; i++)
        {
            if(i%2==0)
            {
                retangulo = retangulo + i;
                if(retangulo==num)
                {
                    printf("%d eh retangular ==>", num);
                    for(int j=2; j<=i; j=j+2)
                    {
                        if(j!=i)
                        {
                            printf("% d +", j);
                        }
                        else
                        {
                            printf(" %d = %d\n\n", j, num);
                        }
                    }
                    result=1;
                    break;
                }
            }
        }
    }

}


char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
