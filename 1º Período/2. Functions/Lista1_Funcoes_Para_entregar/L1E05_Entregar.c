/* 3) Fazer uma fun��o que verifica se um n�mero � um quadrado perfeito. Um n�mero � quadrado perfeito se
possui como raiz quadrada um valor inteiro. A fun��o para obter a raiz quadrada � sqrt() e est� na biblioteca
math.h. Por exemplo, 25 � um quadrado perfeito porque raiz quadrada de 25 � 5. Utilizar essa fun��o para:
a) Verificar se um n�mero, informado pelo usu�rio, � um quadrado perfeito.
b) Mostrar quais n�meros de um intervalo s�o quadrados perfeitos.
Use menu de op��es (com switch-case) e implemente a repeti��o de programa. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

char repete(char rep);

int main()
{
    int linha, coluna;
    char caracter, rep;

    do
    {
        setbuf(stdin,NULL);

        printf("Informe o numero de linhas: ");
        scanf("%d", &linha);
        printf("Informe o numero de colunas: ");
        scanf("%d", &coluna);
        printf("Informe um caracter: ");
        setbuf(stdin, NULL);
        scanf("%c", &caracter);

        desenho(linha, coluna, caracter);

        setbuf(stdin,NULL);
        rep=repete(rep);
    }while(rep=='S' || rep == 's');

    return 0;
}

void desenho(int linha, int coluna, char caracter)
{
    for(int i=0; i<(linha*coluna); i++)
    {
        printf("%c ", caracter);
        if((i+1)%coluna==0)
        {
            printf("\n");
        }
    }
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');

    return(rep);
}
