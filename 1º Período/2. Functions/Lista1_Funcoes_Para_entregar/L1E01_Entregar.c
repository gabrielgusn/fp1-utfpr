/* 1) Fazer uma fun��o que verifica se um n�mero � triangular. Um n�mero natural � triangular quando o
produto de tr�s n�meros naturais consecutivos for igual ao pr�prio n�mero. Por exemplo: 120 � triangular,
pois 4 * 5 * 6 = 120. Use essa fun��o para:
a) Verificar se um n�mero, informado pelo usu�rio, � triangular.
b) Mostrar quais n�meros de um intervalo s�o triangulares.
Use menu de op��es (com switch-case) e implemente a repeti��o de programa. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int triangular(int num);
char repete(char rep);
int teste_triangulo(int num);

int main()
{
    char option, rep;
    int num, limSup, limInf;

    do
    {
        setbuf(stdin, NULL);
        do
        {
            setbuf(stdin, NULL);
            printf("1 - Verifica se um numero eh triangular\n2 - Mostra os numeros triangulares que pertencem a um intervalo\n");
            printf("Opcao: ");
            scanf("%c", &option);
            if(option!='1' && option!='2')
            {
                printf("Opcao Invalida!\n\n");
            }
        }while(option!='1' && option!='2');


        switch(option)
        {
        case '1':
            printf("Informe um numero: ");
            scanf("%d", &num);
            triangular(num);
            break;
        case '2':
            printf("Informe o limite inferior: ");
            scanf("%d", &limInf);
            printf("Informe o limite superior: ");
            scanf("%d", &limSup);
            printf("\n");
            for(int i=limInf; i<=limSup; i++)
            {
                teste_triangulo(i);
            }
            break;
        }
        rep=repete(rep);
    }while(rep=='S' || rep == 's');

    return 0;
}

int triangular(int num)
{
    int triangulo=1;
    printf("\n");
    for(int i=1; i<=num; i++)
    {
        triangulo = i * (i+1) * (i+2);
        if(triangulo==num)
        {
            printf("%d eh triangular ==> %d * %d * %d = %3.d\n", num, i, i+1, i+2, num);
            return 1;
            break;
        }
    }
    printf("%d nao eh um numero triangular\n", num);
}

int teste_triangulo(int num)
{
    int triangulo=1;
    for(int i=1; i<=num; i++)
    {
        triangulo = i * (i+1) * (i+2);
        if(triangulo==num)
        {
            printf("%d eh triangular ==> %d * %d * %d = %d\n", num, i, i+1, i+2, num);
        }
    }
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
