/* 3) Fazer uma fun��o que verifica se um n�mero � um quadrado perfeito. Um n�mero � quadrado perfeito se
possui como raiz quadrada um valor inteiro. A fun��o para obter a raiz quadrada � sqrt() e est� na biblioteca
math.h. Por exemplo, 25 � um quadrado perfeito porque raiz quadrada de 25 � 5. Utilizar essa fun��o para:
a) Verificar se um n�mero, informado pelo usu�rio, � um quadrado perfeito.
b) Mostrar quais n�meros de um intervalo s�o quadrados perfeitos.
Use menu de op��es (com switch-case) e implemente a repeti��o de programa. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

int quadrado(int num);
char repete(char rep);
void intervalo_quadrado();

int main()
{
    char option, rep;
    int num, limSup, limInf;

    do
    {
        setbuf(stdin,NULL);
        do
        {
            printf("1 - Verifica se um numero eh um quadrado perfeito\n2 - Mostra os numeros quadrados perfeitos que pertencem a um intervalo\n");
            printf("Opcao: ");
            setbuf(stdin,NULL);
            scanf("%c", &option);
            if(option != '1' && option != '2')
            {
                printf("Opcao Invalida!\n\n");
            }
        }while(option != '1' && option != '2');

        switch(option)
        {
        case '1':
            printf("Informe um numero: ");
            scanf("%d", &num);
            if(quadrado(num)==1)
            {
                printf("\n%d eh um quadrado perfeito ==> raiz quadrada de %d eh %.0f\n", num, num, sqrt(num));
            }
            else
            {
                printf("\n%d nao eh um quadrado perfeito\n", num);
            }
            break;
        case '2':
            intervalo_quadrado();
            break;
        default:
            printf("Caractere Invalido\n\n");
        }
        setbuf(stdin,NULL);
        rep=repete(rep);
    }while(rep=='S' || rep == 's');

    return 0;
}

int quadrado(int num)
{
    int result, raiz;

    raiz = sqrt(num);
    if((pow(raiz,2))==num)
    {
        result=1;
    }
    else
    {
        result=0;
    }
    return(result);
}

void intervalo_quadrado()
{
    int num, raiz, limInf, limSup;

    do
    {
        printf("Informe o limite inferior: ");
        scanf("%d", &limInf);
        printf("Informe o limite superior: ");
        scanf("%d", &limSup);
        if(limInf>=limSup)
        {
            printf("Limite Inferior nao pode ser maior que o Superior\n\n");
        }
    }while(limInf>=limSup);
    printf("\n");

    for(int num=limInf; num<=limSup; num++)
    {
        raiz = sqrt(num);
        if((pow(raiz,2))==num)
        {
            printf("%d eh um quadrado perfeito ==> raiz quadrada de %d eh %.0f\n", num, num, sqrt(num));
        }
    }
}


char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');

    return(rep);
}
