/* 3) Fazer duas fun��es: uma para calcular o fatorial de um n�mero e a outra para mostrar o fatorial de um
n�mero. Ambas as fun��es devem ser implementadas em um arquivo de cabe�alho chamado "fatorial.h".
Elaborar um programa que use essas fun��es para:
a) Calcular e mostrar o fatorial dos n�meros entre 1 e 8.
b) Calcular e mostrar o fatorial de um valor informado pelo usu�rio. Continuar a leitura enquanto forem
informados valores positivos. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "fatorial.h"

int main()
{
    char option, rep;
    int num, contPrint=0, soma=0, contMed=0;

    do
    {
        printf("A - Fatorial entre 1 e 8\nB - Fatorial de um numero");
        printf("\nOpcao: ");
        setbuf(stdin, NULL);
        scanf("%c", &option);

        option=toupper(option);

        switch(option)
        {
            case 'A':
                fatorial8();
                break;
            case 'B':
                fatorialnum();
                break;
            default:
                printf("Caractere Invalido\n");
                break;
        }
        rep=repete(rep);
        rep=toupper(rep);
    }while(rep=='S');



}

