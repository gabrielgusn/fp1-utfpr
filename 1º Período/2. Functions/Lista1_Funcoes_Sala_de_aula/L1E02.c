/* 2) Fazer uma fun��o para verificar se um n�mero � ou n�o primo. A fun��o ser� implementada em um
arquivo de cabe�alho "primo.h" e dever� retornar 0 se o n�mero for primo ou 1, caso o n�mero n�o seja
primo. Elaborar um programa para usar essa fun��o para:
a) Verificar se um n�mero informado pelo usu�rio � ou n�o um n�mero primo. Validar a entrada para que o
usu�rio informe um n�mero positivo.
b) Mostrar os primos no intervalo entre 1 e 100.
c) Fazer a m�dia dos primos entre 200 e 100. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "primo.h"

int main()
{
    char option, rep;
    int num, contPrint=0, soma=0, contMed=0;

    do
    {
        printf("A - Verificar se um numero eh primo\nB - Primos entre 1 e 100\nC - Media dos primos entre 200 e 100");
        printf("\nOpcao: ");
        setbuf(stdin, NULL);
        scanf("%c", &option);

        option=toupper(option);

        switch(option)
        {
            case 'A':
                printf("Informe um numero: ");
                scanf("%d", &num);
                if(primo(num)==0)
                {
                    printf("%d eh primo\n", num);
                }
                else
                {
                    printf("%d nao eh primo\n", num);
                }
                break;
            case 'B':
                contPrint=0;
                for(int i=1; i<=100; i++)
                {
                    if((primo(i))==0)
                    {
                        printf("%d \t", i);
                        contPrint++;
                    }
                    if(contPrint==10)
                    {
                        printf("\n");
                        contPrint=0;
                    }

                }
                break;
            case 'C':
                for(int i=200; i>=100; i--)
                {
                    if((primo(i))==0)
                    {
                        soma=soma+i;
                        contMed++;
                    }
                }
                printf("Media dos primos entre 200 e 100: %.2f\n", (float)soma/contMed);
                break;
            default:
                printf("Caractere Invalido\n");
                break;
        }
        rep=repete(rep);
        rep=toupper(rep);
    }while(rep=='S');



}

