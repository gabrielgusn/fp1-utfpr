/* 1) Fazer um programa que permite aplicar as opera��es de soma, subtra��o, multiplica��o, divis�o e resto
em dois n�meros fornecidos pelo usu�rio. Cada opera��o � uma fun��o e deve ser acessada a partir de um
menu (use switch case). As fun��es s�o implementadas no pr�prio programa.
a) Soma sem par�metros e sem retorno
b) Subtra��o com par�metros e sem retorno
c) Multiplica��o sem par�metros e com retorno
d) Divis�o com par�metros e com retorno.
e) Resto com par�metros e com retorno. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int soma();
int subtrai(int num1, int num2);
char repete(char rep);
int mult();
float divisao(int num1, float num2);
int resto (int num1, int num2);

int main()
{
    char option, rep;
    int num1, num2;
    do
    {
        printf("A - Adicionar\nS - Subtrair\nM - Multiplicar\nD - Dividir\nR - Resto\n");
        printf("Opcao: ");
        setbuf(stdin, NULL);
        scanf("%c", &option);

        option=toupper(option);

        if(option!= 'A' && option!= 'S' && option!= 'M' && option!= 'D' && option!= 'R')
        {
            printf("Opcao Invalida\n");
        }
        else
        {
            switch(option)
            {
                case 'A':
                    soma();
                    break;
                case 'S':
                    printf("Informe um numero: ");
                    scanf("%d", &num1);
                    printf("Informe outro numero: ");
                    scanf("%d", &num2);
                    subtrai(num1, num2);
                    break;
                case 'M':
                    printf("Multiplicacao: %d\n", mult());
                    break;
                case 'D':
                    printf("Informe um numero: ");
                    scanf("%d", &num1);
                    printf("Informe outro numero: ");
                    scanf("%d", &num2);
                    num2=(float)num2;
                    printf("Divisao: %.2f\n", divisao(num1, num2));
                    break;
                case 'R':
                    printf("Informe um numero: ");
                    scanf("%d", &num1);
                    printf("Informe outro numero: ");
                    scanf("%d", &num2);
                    printf("Resto: %d\n", resto(num1,num2));
            }
        }
        rep=repete(rep);
        rep=toupper(rep);
    }while(rep=='S');

    return 0;
}

int soma()
{
    int num1, num2;

    printf("Informe o primeiro valor: ");
    scanf("%d", &num1);
    printf("Informe o segundo valor: ");
    scanf("%d", &num2);
    printf("Soma: %d\n", num1+num2);

}

int subtrai(int num1, int num2)
{
    printf("Subtracao: %d\n", num1-num2);
}

int mult()
{
    int num1, num2;
    printf("Informe o primeiro valor: ");
    scanf("%d", &num1);
    printf("Informe o segundo valor: ");
    scanf("%d", &num2);

    return(num1*num2);
}

float divisao(int num1, float num2)
{
    return(num1/num2);
}
int resto (int num1, int num2)
{
    return(num1%num2);
}
char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
