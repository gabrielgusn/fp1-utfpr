char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}



void fatorial8()
{
    int prod=1;
    printf("\n");
    for(int i=1; i<=8; i++)
    {
        printf("%d! = ", i);

        for(int j=i; j>=1; j--)
        {
            if(j==1)
            {
                printf("1 = %d\n", prod);
                prod=1;
            }
            else
            {
                printf("%d * ", j);
                prod=prod*j;
            }
        }
    }
}

void fatorialnum()
{
    int num, prod=1;
    do
    {
        do
        {
            printf("\nInforme um numero: ");
            scanf("%d", &num);
        }while(num<0);


        if(num!=0)
        {
            printf("%d! =", num);
            for(int i=num; i>=1; i--)
            {
                if(i==1)
                {
                    printf(" 1 = %d\n", prod);
                    prod=1;
                }
                else
                {
                    printf(" %d *", i);
                    prod = prod * i;
                }
            }
        }
    }while(num!=0);
}

