char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}

int primo(int num)
{
    int contDiv=0;

    for(int i=1; i<=num; i++)
    {
        if(num%i==0)
        {
            contDiv++;
        }
    }
    if(contDiv==2)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
