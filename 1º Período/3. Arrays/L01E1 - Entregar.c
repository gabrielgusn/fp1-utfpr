/* 1) Ler a altura de 5 pessoas, armazenar em um vetor. Validar para que seja informado um valor positivo para
a altura. Identificar e mostrar a maior altura e o �ndice do vetor que essa altura corresponde. Calcular a m�dia
das alturas acima de 1,50 e mostrar essa m�dia. Validar para que n�o seja realizada uma divis�o por zero no
c�lculo da m�dia */
#include <stdio.h>
#include <stdlib.h>

char repete(char rep);


int main()
{
    float height[5], minor = height[0], soma=0;
    int position, num=0;
    char rep;
    do
    {
        soma=0;
        num=0;
        minor = height[0];

        for(int i=0; i<5; i++)
        {
            printf("Informe a altura da pessoa %d: ", i+1);
            scanf("%f", &height[i]);
            if(height[i]>minor)
            {
                minor=height[i];
                position = i;
            }
            if(height[i]>1.50)
            {
                num++;
                soma= soma+height[i];
            }
        }
        printf("\nMaior altura eh %.2f e esta na posicao %d do vetor\n", minor, position);
        printf("Media das alturas maiores que um metro e meio: %.2f\n", soma/num);

        rep=repete(rep);

    }while(rep=='S' || rep=='s');

    return 0;
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
