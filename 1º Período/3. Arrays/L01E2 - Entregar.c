/* 2) Gerar um vetor com 50 elementos (valores aleat�rios), com valor at� 100. Armazenar em um vetor os
n�meros pares e em outro os n�meros �mpares. Mostrar os tr�s vetores. Dica: Declarar os tr�s vetores com
tamanho 50, na pior hip�tese todos os n�meros gerados seriam pares ou �mpares.
Uma solu��o mais otimizada: primeiro percorrer o vetor e contar quantos valores h� de cada tipo e em
seguida declarar os vetores com o tamanho exato, obtido da contagem */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char repete(char rep);


int main()
{
    char rep;
    int num[50];

    do
    {
        int sizePar=0, contPrint=0, sizeImpar=0;
        srand(time(NULL));

        printf("=== VETOR ===\n");

        for(int i=0; i<50; i++)
        {
            num[i] = rand() % 101;
            printf("%3d\t", num[i]);
            contPrint++;
            if(contPrint%10==0)
            {
                printf("\n");
            }
            if(num[i]%2==0)
            {
                sizePar++;
            }
            else
            {
                sizeImpar++;
            }
        }

        int par[sizePar], impar[sizeImpar], aux=0;

        for(int i=0; i<sizePar; i++)
        {
            for(int j=aux; j<50; j++)
            {
                if(num[j]%2==0)
                {
                    par[i]=num[j];
                    aux=1+j;
                    break;
                }
            }
        }

        aux=0;

        for(int i=0; i<sizeImpar; i++)
        {
            for(int j=aux; j<50; j++)
            {
                if(num[j]%2!=0)
                {
                    impar[i]=num[j];
                    aux=1+j;
                    break;
                }
            }
        }


        printf("\n=== PARES ===\n");

        for(int i=0; i<sizePar; i++)
        {
            printf("%3d\t", par[i]);
            contPrint++;
            if(contPrint%10==0)
            {
                printf("\n");
            }
        }
        printf("\n");
        contPrint=0;

        printf("\n=== IMPARES ===\n");

        for(int i=0; i<sizeImpar; i++)
        {
            printf("%3d\t", impar[i]);
            contPrint++;
            if(contPrint%10==0)
            {
                printf("\n");
            }
        }
        printf("\n");

        rep=repete(rep);

    }while(rep=='S' || rep=='s');

    return 0;
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
