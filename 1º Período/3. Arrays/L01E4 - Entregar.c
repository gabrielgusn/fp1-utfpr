/* 4) Gerar um vetor aleat�rio com 10 elementos entre 0 e 50. Verificar se cada um dos elementos armazenados
no vetor � primo. Utilizar, obrigatoriamente, uma fun��o para verificar se o n�mero � primo. Essa fun��o
retorna 0 para indicar que o n�mero � primo e 1 para indicar que n�o � primo. Tratar esse retorno na fun��o
chamadora. Mostrar da seguinte forma: */
#include <stdio.h>
#include <stdlib.h>

char repete(char rep);
int primo(int num);

int main()
{

    char rep;
    do
    {
        int num[10];

        srand(time(NULL));

        printf("==== VETOR ====\n");

        for(int i=0; i<10; i++)
        {
            num[i] = rand() % 51;
            printf("%2d\t", num[i]);
        }

        printf("\n\nINDICE\tNUMERO\tPRIMO\n");

        for(int i=0; i<10; i++)
        {
            if(primo(num[i])==0)
            {
                printf(" %d\t %2d\t Sim\n", i, num[i]);
            }
            else if(primo(num[i])==1)
            {
                printf(" %d\t %2d\t Nao\n", i, num[i]);
            }
        }

        rep=repete(rep);
    }while(rep=='S' || rep=='s');

    return 0;
}

int primo(int num)
{
    int contDiv=0;

    for(int i=1; i<=num; i++)
    {
        if(num%i==0)
        {
            contDiv++;
        }
    }
    if(contDiv==2)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
