/* 3) Deseja-se publicar o n�mero de acertos de cada aluno em uma prova. A prova consta de 10 quest�es,
cada uma com cinco alternativas identificadas por A, B, C, D e E. Para isso s�o dados:
- O cart�o gabarito;
- O n�mero de alunos da turma;
- O cart�o de respostas para cada aluno, contendo o seu n�mero e suas respostas. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char repete(char rep);


int main()
{
    char rep;
    do
    {
        int qtdAluno;

        printf("Digite o numero de alunos: ");
        scanf("%d", &qtdAluno);

        int alunos[qtdAluno];
        char gabarito[10], respAluno[10];

        printf("\n==== GABARITO ====\n");

        for(int i=0; i<10; i++)
        {
            setbuf(stdin, NULL);
            do
            {
                printf("Digite o gabarito da prova para a questao %d: ", i+1);
                setbuf(stdin, NULL);
                scanf("%c", &gabarito[i]);
                gabarito[i]=toupper(gabarito[i]);
                if(gabarito[i]<'A' || gabarito[i]>'E')
                {
                    printf("Caracter Invalido!\n");
                }
            }while(gabarito[i]<'A' || gabarito[i]>'E');
        }

        printf("\n==== RESPOSTAS DOS ALUNOS ====\n");

        for(int i=0; i<qtdAluno; i++)
        {
            int nota=0;
            for(int j=0; j<10; j++)
            {
                setbuf(stdin, NULL);
                do
                {
                    printf("Digite a resposta do aluno %d para a questao %d: ", i+1, j+1);
                    setbuf(stdin, NULL);
                    scanf("%c", &respAluno[j]);
                    respAluno[j]=toupper(respAluno[j]);
                    if(respAluno[j]<'A' || respAluno[j]>'E')
                    {
                        printf("Caracter Invalido!\n");
                    }
                }while(respAluno[j]<'A' || respAluno[j]>'E');
                if(respAluno[j]==gabarito[j])
                {
                    nota++;
                }
            }
            printf("\n\nO aluno %d fez %d pontos\n\n", i+1, nota);
        }
        rep=repete(rep);


    }while(rep=='S' || rep=='s');

    return 0;
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
