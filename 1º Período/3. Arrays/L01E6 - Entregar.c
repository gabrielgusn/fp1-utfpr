/* 6) Gerar um vetor A com 100 elementos entre 10 e 400, inclusive, mostrar o vetor gerado. Em seguida
percorrer o vetor A e contar quantos elementos est�o entre 10 e 100, quantos est�o entre 101 e 200, quantos
est�o entre 201 e 300 e quantos est�o entre 301 e 400. A quantidade ser� armazenada no vetor B com
tamanho 4, para cada uma das respectivas quantidades. Por exemplo, vetorB[0] conter� a quantidade de
valores entre 10 e 100 no vetor A. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char repete(char rep);

int main()
{
    char rep;
    do
    {
        int vetA[100], cont[4]= {0,0,0,0};

        srand(time(NULL));

        printf("\n==== VETOR A ====\n");
        for(int i=0; i<100; i++)
        {
            vetA[i]=(rand()%391)+10;
            printf("%3d\t", vetA[i]);
            if((i+1)%10==0)
            {
                printf("\n");
            }
            if(vetA[i]>=10 && vetA[i]<=100)
            {
                cont[0]++;
            }
            else if(vetA[i]>=101 && vetA[i]<=200)
            {
                cont[1]++;
            }
            else if(vetA[i]>=201 && vetA[i]<=300)
            {
                cont[2]++;
            }
            else if(vetA[i]>=301 && vetA[i]<=400)
            {
                cont[3]++;
            }
        }

        printf("\n==== VETOR B ====\n");

        for(int j=0; j<4; j++)
        {
            printf("%d\t", cont[j]);
        }

        rep=repete(rep);
    }while(rep=='S' || rep=='s');

    return 0;
}

char repete(char rep)
{
    do
    {
        printf("\n\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }
    while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}
