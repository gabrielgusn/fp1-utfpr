/* 5) O que faz o algoritmo a seguir:
declare vetA[30], vetB[30], i, j como inteiro
repetir i = 0, at� i < 30, passo 1
leia vetA[i]
fim-repetir
repetir i = 0, at� i < 30, passo 1
escreva vetA[i]
fim-repetir
j=0;
repetir i = 0, at� i < 30, passo 1
se ( vetA[i] % 2 == 0 ) then
vetB[j] <- vetA[i]
j <- j + 1
fim-se
fim-repetir
repetir i = 0, at� i < j, passo 1
escreva vetB[j]
fim-repetir
Implemente uma solu��o na linguagem C para esse algoritmo. */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Este algoritmo eh responsavel por exibir os numeros pares de um vetor.

char repete(char rep);

int main()
{

    char rep;
    do
    {

        int vetA[30], vetB[30], i, j;

        for(i=0; i<30; i++)
        {
            vetA[i]=rand()%51;
        }

        printf("\n==== VETOR A ====\n");
        for(i=0; i<30; i++)
        {
            printf("%d\t", vetA[i]);
            if((i+1)%10==0)
            {
                printf("\n");
            }
        }

        j=0;

        for(i=0; i<30; i++)
        {
            if(vetA[i]%2==0)
            {
                vetB[j]=vetA[i];
                j++;
            }
        }

        printf("\n==== VETOR B ====\n");
        for(i=0; i<j; i++)
        {
            printf("%d\t", vetB[i]);
            if((i+1)%10==0)
            {
                printf("\n");
            }
        }
        printf("\n");
        rep=repete(rep);

    }while(rep=='S' || rep=='s');

    return 0;
}

char repete(char rep)
{
    do
    {
        printf("\nDeseja repetir?\n");
        setbuf(stdin, NULL);
        scanf("%c", &rep);
        printf("\n");
        if(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n')
        {
            printf("Caractere Invalido\n");
        }
    }
    while(rep!= 'S' && rep!='N' && rep!= 's' && rep!='n');
    return(rep);
}

