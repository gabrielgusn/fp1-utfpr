/* 1) Elaborar um programa que leia um n�mero que representa
uma senha e verifica se a mesma est� correta ou n�o,
comparando-a com 12345 e informa "Acesso autorizado" ou
"Acesso negado", conforme o caso*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include <conio.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int senha;

    printf("Insira a Senha: ");
    scanf("%d", &senha);

    if (senha == 12345)
    {
        printf("Acesso autorizado. \n\n");
    }
    else
    {
        printf("Acesso negado. \n\n");
    }

    system("pause");

    return 0;
}
