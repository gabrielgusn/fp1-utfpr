/* 6) Elaborar um programa que leia um valor que se refere
ŕ altura de uma pessoa e mostre uma mensagem conforme a
tabela a seguir. Utilizar variável do tipo float*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    float height;

    printf("Insira sua altura: ");
    scanf("%f", &height);

    if(height < 1.5)
    {
        printf("Altura abaixo de um metro e cinquenta centímetros. \n\n");
    }
    else if(height >= 1.5 && height <= 1.80)
    {
        printf("Altura entre um metro e cinquenta e um metro e oitenta centímetros. \n\n");
    }
    else
    {
        printf("Altura acima de um metro e oitenta centímetros. \n\n");
    }
    system("pause");

    return 0;
}
