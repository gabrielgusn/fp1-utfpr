/* 5) Escreva um programa que leia um n�mero e verifique se
ele se encontra fora do intervalo entre 5 e 20. Mostre uma
mensagem se o n�mero est� fora desse intervalo.*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{

    setlocale(LC_ALL, "Portuguese");
    int num;

    printf("Insira um n�mero: ");
    scanf("%d", &num);

    if(num < 5 || num > 50)
    {
        printf("O n�mero est� fora do intervalo de 5 � 50. \n\n");
    }
    else
    {
        printf("O n�mero est� no intervalo de 5 � 50. \n\n");
    }

    system("pause");

    return 0;
}

