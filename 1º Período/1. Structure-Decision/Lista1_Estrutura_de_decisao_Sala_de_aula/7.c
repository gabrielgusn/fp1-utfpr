/* 7) Elaborar um programa que l� dois valores, verifica se o
primeiro � m�ltiplo do segundo e escreve a mensagem "S�o
m�ltiplos" ou "N�o s�o m�ltiplos" dependendo da condi��o.
Verificar para que n�o seja realizada uma divis�o por zero.
Nesse caso, informar que n�o � poss�vel realizar uma divis�o
por zero. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2;

    printf("Insira o valor 1: ");
    scanf("%d", &num1);
    printf("Insira o valor 2: ");
    scanf("%d", &num2);

    if((num1 %= num2) == 0)
    {
        printf("S�o m�ltiplos. \n\n");
    }
    else
    {
        printf("N�o s�o m�ltiplos. \n\n");
    }
    system("pause");

    return 0;
}
