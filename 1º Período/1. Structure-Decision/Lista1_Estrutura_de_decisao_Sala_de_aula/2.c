/* 2) Escreva um programa que leia um n�mero e verifique se
ele � maior, menor ou igual a 10. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num;

    printf("Insira um n�mero: ");
    scanf("%d", &num);

    if(num > 10)
    {
        printf("O n�mero %d � maior que 10. \n\n", num);
    }
    else if (num <10)
    {
        printf("O n�mero %d � menor que 10. \n\n", num);
    }
    else
    {
        printf("O n�mero %d � igual a 10. \n\n", num);
    }
    system("pause");

    return 0;
}
