/* 8) Fa�a um programa que leia tr�s notas de um aluno e
calcule a m�dia ponderada, com os pesos 1, 3 e 4,
respectivamente, e:
a) Se a m�dia obtida est� entre 6 a 10 informar que o aluno
est� aprovado;
b) Se a m�dia obtida est� entre 4 e 5,9 informar que o aluno
est� em recupera��o. Nesse caso, ler a nota de
recupera��o e calcular a m�dia final (que � a m�dia entre a
m�dia anual e a nota de recupera��o);
b.1) Se a m�dia final � menor que 5 informar que o aluno
est� reprovado ap�s recupera��o;
b.2) Se � igual ou maior que 5 informar que o aluno est�
aprovado ap�s recupera��o;
c) Se a m�dia obtida � menor que 4 informar que o aluno est�
reprovado antes da recupera��o */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float n1, n2, n3, media;
    printf("Insira a nota 1: ");
    scanf("%f", &n1);
    printf("Insira a nota 2: ");
    scanf("%f", &n2);
    printf("Insira a nota 3: ");
    scanf("%f", &n3);

    media = ((n1*1)+(n2*2)+(n3*3))/6;

    if(media >= 6 && media <= 10)
    {
        printf("Aprovado \n\n ");
    }
    else if (media >=4 && media <=5.9)
    {
        printf("Insira a nota de Recupera��o: \n\n");
        scanf("%f", &n1);
        n2 = (media + n1)/2;
        printf("M�dia Final = %.2f \n\n", n2);

        if(n2 < 5)
        {
            printf("Reprovado ap�s recupera��o. \n\n");
        }
        else if(n2 >= 5)
        {
            printf("Aprovado ap�s recupera��o. \n\n");
        }
    }
    else if (media < 4)
    {
        printf("Reprovado antes da recupera��o. \n\n");
    }
    else
    {
        printf("N�o foi poss�vel calcular a m�dia pois as notas inseridas s�o inv�lidas. \n\n");
    }
    system("pause");

    return 0;
}
