/* 3) Elaborar um programa que leia um caractere. Se o
caractere informado for �F� ou �f� mostrar a mensagem �pessoa
f�sica�, se informado �J� ou �j� mostrar a mensagem �pessoa
jur�dica� ou mostrar "caractere inv�lido" para qualquer outro
caractere.*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    char caracter, inv;

    printf("Informe um caractere: ");
    scanf("%c", &caracter);

    if(caracter == 'F' || caracter == 'f')
    {
        printf("Pessoa F�sica. \n\n");
    }
    else if(caracter == 'J' || caracter == 'j')
    {
        printf("Pessoa Jur�dica. \n\n");
    }
    else
    {
        printf("Caractere Inv�lido. \n\n");
    }

    system("pause");

    return 0;
}
