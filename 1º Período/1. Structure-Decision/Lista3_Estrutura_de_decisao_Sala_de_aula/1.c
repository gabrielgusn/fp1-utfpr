/* 1) O código a seguir tem o objetivo de obter o resto da divisão do número maior pelo menor informados 
(independentemente da ordem informada) e informar que não é possível realizar a divisão caso o divisor seja zero. 
Faça os ajustes necessários no código para que esses objetivos sejam alcançados.*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2, resto;
 
    printf("Informe o primeiro valor: ");
    scanf("%d", &num1);
    setbuf(stdin, NULL);
    printf("Informe o segundo valor: ");
    scanf("%d", &num2);
 
    if(num1 > num2)
    {
        resto = num1 % num2;
        printf("O resto da divisao eh %d\n", resto);
    }
    else if(num2>num1)
    {
        resto = num2 % num1;
        printf("O resto da divisao eh %d\n", resto);
    }
    else if(num1 || num2 == 0)
    {
        printf("Nao eh possivel realizar divisao por zero\n");
    }

    system("pause");

    return 0;
}