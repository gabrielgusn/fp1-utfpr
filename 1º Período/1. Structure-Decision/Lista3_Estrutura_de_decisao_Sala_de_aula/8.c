/* 8) Implemente um programa que adivinhe o "n�mero m�gico" entre 0 e 10. O programa dever� imprimir a mensagem
"Certo! %d � o n�mero m�gico" quando o jogador acerta o n�mero m�gico, a mensagem "Errado, muito alto", caso o
jogador tenha digitado um n�mero maior que o n�mero m�gico e a mensagem "Errado, muito baixo", caso o jogador
tenha digitado um n�mero menor que o n�mero m�gico. O n�mero m�gico � produzido usando o gerador de n�meros
rand�micos de C (fun��o rand(), que exige o uso da biblioteca stdlib.h)*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num_inf, num_rnd, i;
    printf("Adivinhe qual eh o numero de 1 a 10\n");

    srand(time(NULL));
    num_rnd = (rand() % 10)+1;

    for(i=10; i>0; i--)
    {
        printf("%d tentativas restantes\n", i);
        printf("\nDigite um numero: ");
        scanf("%d",&num_inf);

        if(num_inf>num_rnd)
        {
            printf("Errado, muito alto\n");
        }
        else if(num_inf<num_rnd)
        {
            printf("Errado, muito baixo\n");
        }
        else if(num_inf=num_rnd)
        {
            printf("Voce acertou o numero era %d\n", num_rnd);
            i=0;
        }
    }

    system ("pause");

    return 0;
}

