/* 2) Ler dois números inteiros e informar:a) Se ambos são divisíveis por 5.
b) Se pelo menos um deles é divisível por 5.
c) Se ambos são pares.
d) Se pelo menos um deles é ímpar.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2;

    printf("Insira o número 1: ");
    scanf("%d", &num1);
    printf("Insira o número 2: ");
    scanf("%d", &num2);

    if (num1%5==0 && num2%5==0)
    {
        printf("%d e %d são divisíveis por 5 \n", num1, num2);
    }
    else if (num1%5==0 || num2%5==0)
    {
        printf("%d ou %d é divisível por 5 \n", num1, num2);
    }
    if (num1%2==0 && num2%2==0)
    {
        printf("%d e %d são pares \n", num1, num2);
    }
    else if (num1%2==0 || num2%2==0)
    {
        printf("%d ou %d é par \n", num1, num2);
    }

    system("pause");

    return 0;
}