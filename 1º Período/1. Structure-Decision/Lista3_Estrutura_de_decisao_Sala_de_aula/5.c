/* 5) Uma árvore de decisão obtém a decisão pela execução de uma sequência de testes. Cada nó interno da árvore 
corresponde a um teste do valor de uma das propriedades e os ramos deste nó são identificados com os possíveis 
valores do teste. Cada nó folha da árvore especifica o valor de retorno se a folha for atingida. A figura a seguir 
apresenta um exemplo fictício de árvore de decisão, tomando atributos de clientes de uma instituição financeira. 
Elabore um programa que implemente essa árvore de decisão. As entradas podem ser do tipo char, portanto, considere 
a primeira letra de cada palavra como entrada. Caso o usuário informe um caractere diferente dos aceitáveis, apresente 
a mensagem "Caractere inválido". */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include <ctype.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    char entry;
    
    printf("O cliente possui saldo em conta corrente(P ou N): ");
    scanf("%c", &entry);
    entry = toupper(entry);
    setbuf(stdin, NULL);

    switch(entry)
    {
        case 'P':
        printf("Cliente sem risco. \n\n");
        break;
        case 'N':
        printf("Você possui aplicações? (P ou N)\n");
        scanf("%c", &entry);
        entry = toupper(entry);
        setbuf(stdin, NULL);
        switch(entry)
        {
            case 'P':
            printf("Cliente sem risco. \n\n");
            break;
            case 'N':
            printf("Cliente com risco. \n\n");
            break;
            default:
            printf("Caractere inválido. \n\n");
            break;
        }
        break;
        default:
        printf("Caractere inválido. \n\n");
        break;
    }
    system("pause");

    return 0;
}