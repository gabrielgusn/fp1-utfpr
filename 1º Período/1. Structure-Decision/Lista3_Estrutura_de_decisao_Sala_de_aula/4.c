/* 4) Um ano é bissexto se for divisível por 4 e não for divisível por 100. Também são bissextos os divisíveis por 400. 
Escreva um programa que determina se um ano informado pelo usuário é bissexto.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int ano;
    
    printf("Informe o ano: ");
    scanf("%d", &ano);

    if (ano%4==0 && ano%100!=0 || ano%400==0)
    {
        printf("O ano %d é bissexto. \n\n", ano);
    }
    else
    {
        printf("O ano %d não é bissexto \n\n", ano);
    }
    
    system("pause");

    return 0;
}