/* 6) Crie um programa que leia dia, mês e ano separadamente e imprima se a data é válida ou não. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int d, m, a;
    char rep;

    do{
    printf("Insira o dia: ");
    scanf("%d", &d);

        if(d>=1 && d<=31)
        {   do{
            printf("Insira o mes: ");
            scanf("%d", &m);

            if(m>=1 && m<=12)
            {
                printf("Insira o ano: ");
                scanf("%d", &a);
                if(m<=7 && m>=1 && m%2==0 && d<=30)
                {
                    if(m==2 && d==29)
                    {
                      //bissexto
                        if(a%4==0 && a%100!=0 || a%400==0 && m==2 & d==29)
                        {
                            printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                        }
                        else
                        {
                            printf("A data %d/%d/%d nao eh valida. \n\n", d, m, a);
                        }
                    }
                    else if(m==2 && d<=28)
                    {
                         printf("A data %d/%d/%d eh valida. \n\n",d,m,a);
                    }

                }
                else if(m<=7 && m%2!=0 && d<=31)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else if(m>=8 && m<=12 && m%2==0 && d<=31)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else if(m>=8 && m<=12 && m%2!=0 && d<=30)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else
                {
                    printf("A data %d/%d/%d nao eh valida \n\n.", d, m, a);
                }
            }
            else
            {
                printf("Mes %d Invalido. \n\n", m);
            }
            setbuf(stdin,NULL);
            printf("Deseja Tentar Novamente? (S ou N)\n");
            scanf("%c", &rep);
            rep = toupper(rep);
            }while(rep=='S');
        }
        else
        {
            printf("Dia %d invalido. \n\n", d);

        }
        setbuf(stdin,NULL);
        printf("Deseja Tentar Novamente? (S ou N)\n");
        scanf("%c", &rep);
        rep = toupper(rep);
    }while(rep=='S');



    system("pause");

    return 0;
}
