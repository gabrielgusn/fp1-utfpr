/*7) Crie um programa que leia uma data no formato ddmmaaaa e imprima se a data � v�lida ou n�o.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int d, m, a, data;

    printf ("Insira uma data (ddmmaaaa): ");
    scanf ("%d", &data);

    d = data/1000000;
    m = (data%1000000)/10000;
    a = (data%1000000)%10000;

   if(d>=1 && d<=31)
        {
            if(m>=1 && m<=12)
            {
                if(m<=7 && m>=1 && m%2==0 && d<=30)
                {
                    if(m==2 && d==29)
                    {
                      //bissexto
                        if(a%4==0 && a%100!=0 || a%400==0 && m==2 & d==29)
                        {
                            printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                        }
                        else
                        {
                            printf("A data %d/%d/%d nao eh valida. \n\n", d, m, a);
                        }
                    }
                    else if(m==2 && d<=28)
                    {
                         printf("A data %d/%d/%d eh valida. \n\n",d,m,a);
                    }

                }
                else if(m<=7 && m%2!=0 && d<=31)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else if(m>=8 && m<=12 && m%2==0 && d<=31)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else if(m>=8 && m<=12 && m%2!=0 && d<=30)
                {
                    printf("A data %d/%d/%d eh valida. \n\n", d, m, a);
                }
                else
                {
                    printf("A data %d/%d/%d nao eh valida \n\n.", d, m, a);
                }
            }
            else
            {
                printf("Mes %d Invalido. \n\n", m);
            }
        }
        else
        {
            printf("Dia %d invalido. \n\n", d);
        }


    system ("pause");

    return 0;
}
