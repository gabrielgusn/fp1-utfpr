/*3)  Elabore  um  programa  que  leia  o  dia  e  o  m�s  de  nascimento  de  uma  pessoa  e  determine  o  seu  signo  conforme  a
tabela a seguir:
Se informada uma data que n�o corresponde aos intervalos indicados, informar que a data � inv�lida. */

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main(void)
{
    setlocale(LC_ALL, "Portuguese");
    int d, m;

    printf("Informe o dia do nascimento: ");
    scanf("%d", &d);
    printf("Informe o mes do nascimento: ");
    scanf("%d", &m);

    if(m==12 && d>=22 && d<=31)
    {
        printf("Seu signo eh capricornio\n");
    }
    else if(m==1 && d>=1 && d<=20)
    {
        printf("Seu signo eh capricornio\n");
    }
    else if(m==1 && d>=21 && d<=31)
    {
        printf("Seu signo eh aquario\n");
    }
    else if(m==2 && d>=1 && d<=19)
    {
        printf("Seu signo eh aquario\n");
    }
    else if(m==2 && d>=20 && d<=29)
    {
        printf("Seu signo eh peixes\n");
    }
    else if(m==3 && d>=1 && d<=20)
    {
        printf("Seu signo eh peixes\n");
    }
    else if(m==3 && d>=21 && d<=31)
    {
        printf("Seu signo eh aries\n");
    }
    else if(m==4 && d>=1 && d<=20)
    {
        printf("Seu signo eh aries\n");
    }
    else if(m==4 && d>=21 && d<=30)
    {
        printf("Seu signo eh touro\n");
    }
    else if(m==5 && d>=1 && d<=20)
    {
        printf("Seu signo eh touro\n");
    }
    else if(m==5 && d>=21 && d<=31)
    {
        printf("Seu signo eh gemeos\n");
    }
    else if(m==6 && d>=1 && d<=20)
    {
        printf("Seu signo eh gemeos\n");
    }
    else if(m==6 && d>=21 && d<=30)
    {
        printf("Seu signo eh cancer\n");
    }
    else if(m==7 && d>=1 && d<=21)
    {
        printf("Seu signo eh cancer\n");
    }
    else if(m==7 && d>=22 & d<=31)
    {
        printf("Seu signo eh leao\n");
    }
    else if(m==8 && d>=1 && d<=22)
    {
        printf("Seu signo eh leao\n");
    }
    else if(m==8 && d>=23 && d<=31)
    {
        printf("Seu signo eh virgem\n");
    }
    else if(m==9 && d>=1 && d<=22)
    {
        printf("Seu signo eh virgem\n");
    }
    else if(m==9 && d>=23 && d<=30)
    {
        printf("Seu signo eh libra\n");
    }
    else if(m==10 && d>=1 && d<=22)
    {
        printf("Seu signo eh libra\n");
    }
    else if(m==10 && d>=23 && d<=31)
    {
        printf("Seu signo eh escorpiao\n");
    }
    else if(m==11 && d>=1 && d<=21)
    {
        printf("Seu signo eh escorpiao\n");
    }
    else if(m==11 && d>=22 && d<=30)
    {
        printf("Seu signo eh sagitario\n");
    }
    else if(m==12 && d>=1 && d<=21)
    {
        printf("Seu signo eh sagitario\n");
    }
    else
    {
        printf("Data invalida\n");
    }

    system ("pause");

    return 0;
}
