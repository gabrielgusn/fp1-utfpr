/* 7) Ler um n�mero e utilizando uma estrutura if else if else if... (obrigatoriamente encadeada) informar se ele:
a) � divis�vel por 5, por 3 ou por 2;
Exemplo: 30 � divis�vel por 2, 3 e 5.
b) � divis�vel somente por 5 e por 3; por 5 e por 2; ou por 3 e por 2;
Exemplo: 15 � divis�vel somente por 3 e por 5.
Exemplo: 10 � divis�vel somente por 5 e por 2.
Exemplo: 6 � divis�vel somente por 3 e por 2.
c) � divis�vel somente por 5, por 3 ou por 2;
Exemplo: 25 � divis�vel somente por 5
d) N�o � divis�vel por nenhum destes;
Exemplo: 7 n�o � divis�vel por 5, por 3 ou por 2*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int val;

    printf("Insira um valor inteiro: ");
    scanf("%d", &val);
    setbuf(stdin, NULL);

    if((val % 5) == 0 && (val % 3) == 0 &&(val % 2) == 0)
    {
        printf("%d � divisivel por 5, por 3 e por 2 \n\n", val);
    }
    else if((val % 5) == 0 && (val % 3) == 0 && (val % 2) != 0)
    {
         printf("%d � divisivel por 5 e por 3 \n\n", val);
    }
    else if((val % 5) == 0 && (val % 3) != 0 &&(val % 2) == 0)
    {
         printf("%d � divisivel por 5 e por 2 \n\n", val);
    }
     else if((val % 5) != 0 && (val % 3) == 0 &&(val % 2) == 0)
    {
         printf("%d � divisivel por 3 e por 2 \n\n", val);
    }
     else if((val % 5) == 0 && (val % 3) != 0 &&(val % 2) != 0)
    {
         printf("%d � divisivel apenas por 5 \n\n", val);
    }
    else if((val % 5) != 0 && (val % 3) == 0 &&(val % 2) != 0)
    {
         printf("%d � divisivel apenas por 3 \n\n", val);
    }
    else if((val % 5) != 0 && (val % 3) != 0 &&(val % 2) == 0)
    {
         printf("%d � divisivel apenas por 2 \n\n", val);
    }
    else
    {
         printf("%d n�o � divisivel por 5, por 3 e por 2 \n\n", val);
    }
    system("pause");

    return 0;
}
