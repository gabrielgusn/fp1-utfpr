/*5) Calcular o imposto de renda de uma pessoa de acordo com a seguinte tabela:
Renda anual                         Al�quota*
At� R$ 10.000,00                        0%
> R$ 10.000,00 e <= R$ 25.000,00        10%
Acima de R$ 25.000,00                   25%
*Al�quota � o percentual para realizar o c�lculo do imposto de renda a ser pago.
Se informado valor negativo, n�o realizar o c�lculo e informar o usu�rio */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float renda, IR;

    printf("===CALCULADORA DE IR===\n\n");

    printf("Insira o seu sal�rio: R$ ");
    scanf("%f", &renda);

    if(renda>=0)
    {
        if(renda<=10000)
        {
            printf("N�o � necess�rio pagar IR at� R$10.000,00\n\n");
        }
        else if(renda>10000 && renda<=25000)
        {
            IR=renda*0.10;
            printf("O IR a ser pago � R$%.2f\n\n", IR);
        }
        else
        {
            IR=renda*0.25;
            printf("O IR a ser pago � R$%.2f\n\n", IR);
        }
    }
    else
    {
        printf("N�o � poss�vel realizar o c�lculo pois R$.2f � um valor negativo.\n\n");
    }

    system("pause");

    return 0;
}
