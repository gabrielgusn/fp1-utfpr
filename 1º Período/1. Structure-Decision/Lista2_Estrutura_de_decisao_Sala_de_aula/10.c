/* 10) Fa�a um programa que solicite ao usu�rio o valor do sal�rio de um funcion�rio e apresente o menu a
seguir e permita ao usu�rio escolher a op��o desejada e mostre o resultado. Verifique a possibilidade de
op��o inv�lida e n�o se preocupe com restri��es, como sal�rio negativo. Use switch - case, if e if - else para a
solu��o.Menu de op��es:
1 � Imposto
2 � Novo sal�rio
3 � Classifica��o
Digite a op��o desejada*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float sal;
    int opt;

    printf("Insira o valor do sal�rio: R$ ");
    scanf("%f", &sal);
    printf("Menu de op��es \n 1 - Imposto \n 2 - Novo Sal�rio \n 3 - Classifica��o \n");
    printf("Digite a op��o desejada:");
    scanf("%d", &opt);

    switch(opt)
    {
        case 1:
            if (sal<1000)
            {
                printf("Sal�rio L�quido ap�s 5%% de IR: R$ %.2f \n", sal-(sal*0,05));
            }
            else if (sal>=1000 && sal<=1500)
            {
                printf("Sal�rio L�quido ap�s 10%% de IR: R$ %.2f \n", sal-(sal*0,10));
            }
            if (sal>1500)
            {
                printf("Sal�rio L�quido ap�s 15%% de IR: R$ %.2f \n", sal-(sal*0,15));
            }
        break;
        case 2:
            if (sal<1000)
            {
                printf("Sal�rio L�quido ap�s aumento: R$ %.2f \n", sal+75);
            }
            else if (sal>=1000 && sal<=1500)
            {
                printf("Sal�rio L�quido ap�s aumento: R$ %.2f \n", sal+100);
            }
            if (sal>1500)
            {
                printf("Sal�rio L�quido ap�s aumento: R$ %.2f \n", sal+150);
            }
        break;
        case 3:
            if(sal >= 1000)
            {
                printf("Categoria A. \n\n");
            }
            else
            {
                printf("Categoria B. \n\n");
            }
        break;
    }

    system("pause");

    return 0;
}
