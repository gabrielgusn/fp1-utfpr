/* 4) Ler o gênero (F ou f para feminino, M ou m para masculino. Para qualquer outro caractere informar que é 
inválido e finalizar o programa). Se informado um caractere válido, ler a altura da pessoa e calcular e mostrar 
o seu peso ideal, utilizando as seguintes fórmulas:
a) Para homens: (72.7 * h) - 58;
b) Para mulheres: (62.1 * h) - 44.7. */
//professora, escrevi esse em inglês pois estou traduzindo meus códigos para por no git

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include <ctype.h>

int main()
{
    //setlocale(LC_ALL, "Portuguese");
    char gender;
    float h, idw; //idw: ideal weight, h: height

    printf("Insert your gender(M/F): ");
    scanf("%c", &gender);
    printf("Insert your height(in m): ");
    scanf("%f", &h);

    gender = toupper(gender);
    switch(gender) 
    {
    case 'M':
        idw = (72.7 * h)-58;
        printf("Your ideal weight is %.2f \n\n", idw);
        break;
    case 'F':
        idw = (62.1 * h)-44.7;
         printf("Your ideal weight is %.2f \n\n", idw);
        break;
    default:
        printf("%c is an invalid character, please type F or M for your gender.\n\n", gender);
        break;
    }

    system("pause");

    return 0;
}
