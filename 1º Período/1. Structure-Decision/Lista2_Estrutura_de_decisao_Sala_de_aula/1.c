/* 1) Ler tr�s valores inteiros diferentes, encontrar e mostrar o menor deles. Deve ser criada uma vari�vel para
armazenar o menor n�mero. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2, num3, min;
    printf("Insira o primeiro valor int: ");
    scanf("%d", &num1);
    printf("Insira o segundo valor int: ");
    scanf("%d", &num2);
    printf("Insira o terceiro valor int: ");
    scanf("%d", &num3);

    if(num1<num2 && num1<num3)
    {
        min = num1;
        printf("O menor n�mero � %d \n\n", min);
    }
    else if(num2<num1 && num2<num3)
    {
        min = num2;
        printf("O menor n�mero � %d \n", min);
    }
    else if(num3<num2 && num3<num1)
    {
        min = num3;
        printf("O menor n�mero � %d \n", min);
    }



    system("pause");

    return 0;
}
