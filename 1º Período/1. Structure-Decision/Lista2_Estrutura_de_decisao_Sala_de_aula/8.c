/*8) Fazer o programa para o algoritmo representado no fluxograma  */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float salBa, salBr, grat, ir, salL;

    printf("Insira o Sal�rio Base: R$ ");
    scanf("%f", &salBa);
    printf("Insira a gratifica��o: ");
    scanf("%f", &grat);

    salBr = salBa+ (salBa*(grat/100));

    if(salBr<1000)
    {
        ir = salBr *(15/100);
        salL = salBr - ir;
        printf("Sal�rio L�quido: %.2f \n\n", salBa);
    }
    else
    {
        ir = salBr *(20/100);
        salL = salBr - ir;
        printf("Sal�rio L�quido: %.2f \n\n", salBa);
    }

    system("pause");
    return 0;
}
