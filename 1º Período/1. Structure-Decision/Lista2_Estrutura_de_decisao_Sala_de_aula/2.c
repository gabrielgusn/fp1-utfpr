/* 2) Ler tr�s valores inteiros diferentes, encontrar e mostrar o n�mero do meio. Deve ser criada uma vari�vel
para armazenar o n�mero do meio */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2, num3, mid;

    printf("Insira o primeiro valor int: ");
    scanf("%d", &num1);
    printf("Insira o segundo valor int: ");
    scanf("%d", &num2);
    printf("Insira o terceiro valor int: ");
    scanf("%d", &num3);

    //copiar ||

    if(num1>num2 && num1<num3 || num1<num2 && num1>num3)
    {
        mid = num1;
        printf("Dentre %d, %d e %d, o n�mero do meio � %d.\n\n", num1, num2, num3, mid);
    }
    else if(num2>num1 && num2<num3 || num2<num1 && num2>num3)
    {
        mid = num2;
        printf("Dentre %d, %d e %d, o n�mero do meio � %d.\n\n", num1, num2, num3, mid);
    }
    else if(num3>num1 && num3<num2 || num3<num1 && num3>num2)
    {
        mid  = num3;
        printf("Dentre %d, %d e %d, o n�mero do meio � %d.\n\n", num1, num2, num3, mid);
    }

    system("pause");

    return 0;
}
