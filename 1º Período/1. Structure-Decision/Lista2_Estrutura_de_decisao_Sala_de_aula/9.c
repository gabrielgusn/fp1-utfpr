/* 9) N�meros pal�ndromos s�o aqueles que escritos da direita para a esquerda ou da esquerda para a direita
tem o mesmo valor. Ex.: 9229, 4554, 9779. Fazer um programa que dado um n�mero de 4 d�gitos, calcular e
escrever se este n�mero � ou n�o pal�ndromo */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
     int un, dz, cn, ml, num;

    printf ("Insira um n�mero de 4 d�gitos:");
    scanf ("%d", &num);

    ml = num/1000;
    cn = num%1000/100;
    dz = num%100/10;
    un = num%10;

    if (ml==un && dz==cn)
    {
        printf ("%d � pal�ndromo.\n", num);
    }
    else
    {
        printf ("%d n�o � pal�ndromo.\n", num);
    }
    system("pause");

    return 0;
}
