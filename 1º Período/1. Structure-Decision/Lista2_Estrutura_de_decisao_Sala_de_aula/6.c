/* 6) Ler tr�s valores inteiros que representam os lados de um tri�ngulo e determinar se esses valores podem
formar um tri�ngulo (obs.: para ser um tri�ngulo cada lado deve ser menor que a soma dos outros dois lados).
Se for um tri�ngulo, determinar o seu tipo: equil�tero (todos os lados iguais), is�sceles (dois lados iguais) e
escaleno (todos os lados diferentes)*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int l1, l2, l3;

    printf("Insira o lado 1 do tri�ngulo: ");
    scanf("%d", &l1);
    printf("Insira o lado 2 do tri�ngulo: ");
    scanf("%d", &l2);
    printf("Insira o lado 3 do tri�ngulo: ");
    scanf("%d", &l3);

    if(l1<=(l2+l3) && l2<=(l1+l3) && l3<=(l1+l2))
    {
        if(l1==l2 && l1==l3)
        {
            printf("� um Tri�ngulo Equil�tero.\n\n");
        }
        else if(l1==l2 || l1==l3 || l2==l3)//(l1==l2 && l1!=l3 || l1==l3 && l1!=l2 || l2==l3)
        {
            printf("� um Tri�ngulo Is�sceles.\n\n");
        }
        else
        {
            printf("� um Tri�ngulo Escaleno.\n\n");
        }
    }
    else
    {
        printf("Estes lados n�o representam um tri�ngulo.\n\n");
    }

    system("pause");

    return 0;
}
