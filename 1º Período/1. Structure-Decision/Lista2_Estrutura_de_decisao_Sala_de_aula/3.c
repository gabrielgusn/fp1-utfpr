/* 3) Ler tr�s valores inteiros diferentes e coloc�-los em ordem crescente. Os valores devem ser apresentados
com uma instru��o:
printf("Menor: %d Meio: %d Maior: %d\n", menor, meio, maior);
Sugest�o: Dividir o problema em partes: encontrar o maior, o menor e o do meio separadamente. Armazenar
os valores em vari�vel e mostr�-los com uma �nica instru��o. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int num1, num2, num3, menor, meio, maior;

    printf("Insira o primeiro valor int: ");
    scanf("%d", &num1);
    printf("Insira o segundo valor int: ");
    scanf("%d", &num2);
    printf("Insira o terceiro valor int: ");
    scanf("%d", &num3);

    if(num1<num2 && num1<num3)
    {

        if(num2>num1 && num2<num3)
        {
            meio = num2 ;
            maior =num3;
        }
        else if(num2>num3)
        {
            maior = num2;
            meio = num3;
        }
        menor = num1;
        printf("Menor: %d Meio: %d Maior: %d\n", menor, meio, maior);
    }
    else if(num2<num1 && num2<num3)
    {

        if(num1>num2 && num1<num3)
        {
            meio = num1;
            maior = num3;
        }
        else if(num1>num3)
        {
            maior = num1;
            meio = num3;
        }
        menor=num2;
        printf("Menor: %d Meio: %d Maior: %d\n", menor, meio, maior);
    }
    else
    {

        if(num1>num3 && num1<num2)
        {
             meio = num1;
            maior = num2;
        }
        else if(num1>num3)
        {
            maior = num1;
            meio = num2;
        }
        menor=num3;
        printf("Menor: %d Meio: %d Maior: %d\n", menor, meio, maior);
    }

    system("pause");

    return 0;
}
