/*3) A figura a seguir representa uma �rvore de decis�o para identificar se um paciente est� saud�vel ou
doente. Elabore um programa que implementa essa �rvore de decis�o */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include <ctype.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    char std;
    int temp;

    printf("Insira 0 para nao e 1 para sim\n\n");
    printf("Paciente se sente bem?\n");
    scanf("%c", &std);
     setbuf(stdin, NULL);

    if(std == '1')
    {
        printf("Saudavel\n\n");
    }
    else if(std == '0')
    {
        printf("Paciente tem dor? \n");
        scanf("%c", &std);
        setbuf(stdin, NULL);
        if(std == 1)
        {
            printf("Doente\n\n");
        }
        else if(std == '0')
        {
            printf("Temperatura do paciente: \n");
            scanf("%d", &temp);

            if (temp > 37)
            {
                printf("Doente \n\n");
            }
            else
            {
                printf("Saudavel\n\n");
            }
        }
    }
    system("pause");

    return 0;
}
