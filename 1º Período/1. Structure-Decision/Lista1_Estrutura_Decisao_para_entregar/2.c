/* 2) Faça um programa que apresente o menu a seguir e permita ao usuário escolher a opção desejada,
receba os dados necessários para executar a operação e mostre o resultado.
Menu de opções:
1 – Mostra os números em ordem crescente
2 – Mostra os números em ordem decrescente
3 - Mostra os números que são múltiplos de 2
Digite a opção desejada: */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    //setlocale(LC_ALL, "Portuguese");

    int op, num1, num2, num3, menor, meio, maior;

    printf(" Menu de opcoes:\n\n ");
    printf("1 - Mostra os numeros em ordem crescente\n 2 - Mostra os numeros em ordem decrescente\n 3 - Mostra os numeros que sao multiplos de 2\n  ");
    printf("Digite a Opcao desejada: ");
    scanf("%d", &op);


    switch(op)
    {
        case 1:
         printf("Insira o valor 1: ");
        scanf("%d", &num1);
        printf("Insira o valor 2: ");
        scanf("%d", &num2);
        printf("Insira o valor 3: ");
        scanf("%d", &num3);

        if(num1<num2 && num1<num3)
        {
            menor = num1;
            if (num2<num3)
            {
                meio = num2;
                maior = num3;
            }
            else
            {
                meio = num3;
                maior = num2;
            }
        }
        else if(num2<num1 && num2<num3)
        {
            menor = num2;
            if (num1<num3)
            {
                meio = num1;
                maior = num3;
            }
            else
            {
                meio = num3;
                maior = num1;
            }
        }
        else
        {
        menor = num3;
        if (num1<num2)
            {
                meio = num1;
                maior = num2;
            }
            else
            {
                meio = num2;
                maior = num1;
            }
        }
        printf("Ordem Crescente: %d, %d, %d \n\n", menor, meio, maior);
        break;
        case 2:
         printf("Insira o valor 1: ");
        scanf("%d", &num1);
        printf("Insira o valor 2: ");
        scanf("%d", &num2);
        printf("Insira o valor 3: ");
        scanf("%d", &num3);

        if(num1<num2 && num1<num3)
        {
            menor = num1;
            if (num2<num3)
            {
                meio = num2;
                maior = num3;
            }
            else
            {
                meio = num3;
                maior = num2;
            }
        }
        else if(num2<num1 && num2<num3)
        {
            menor = num2;
            if (num1<num3)
            {
                meio = num1;
                maior = num3;
            }
            else
            {
                meio = num3;
                maior = num1;
            }
        }
        else
        {
        menor = num3;
        if (num1<num2)
            {
                meio = num1;
                maior = num2;
            }
            else
            {
                meio = num2;
                maior = num1;
            }
        }
        printf("Ordem Decrescente: %d, %d, %d \n\n", maior, meio, menor);
        break;
        case 3:
         printf("Insira o valor 1: ");
        scanf("%d", &num1);
        printf("Insira o valor 2: ");
        scanf("%d", &num2);
        printf("Insira o valor 3: ");
        scanf("%d", &num3);

         if (num1%2==0 && num2%2==0 && num3%2==0)
        {
            printf("Os numeros %d, %d e %d sao multiplos de 2. \n\n", num1, num2, num3);
        }
        else if (num1%2==0 && num2%2==0 && num3%2!=0)
        {
            printf("Apenas os numeros %d e %d sao multiplos de 2. \n\n", num1, num2);
        }
        else if (num1%2==0 && num2%2!=0 && num3 %2==0)
        {
            printf("Apenas os numeros %d e %d sao multiplos de 2. \n\n", num1, num3);
        }
        else if (num1%2!=0 && num2%2==0 && num3%2==0)
        {
            printf("Apenas os numeros %d e %d sao multiplos de 2. \n\n", num2, num3);
        }
        else if (num1 % 2 ==0 && num2 %2 != 0 && num3 % 2 != 0)
        {
            printf("Apenas o numero %d eh multiplo de 2. \n\n", num1);
        }
        else if (num1 % 2 !=0 && num2 %2 == 0 && num3 % 2 != 0)
        {
            printf("Apenas o numero %d eh multiplo de 2. \n\n", num2);
        }
        else if (num1 % 2 !=0 && num2 %2 != 0 && num3 % 2 == 0)
        {
            printf("Apenas o numero %d eh multiplo de 2. \n\n", num3);
        }
        else
        {
            printf("Nenhum numero informado eh multiplo de 2. \n\n");
        }
        break;
        default :
        printf("Opcao Invalida \n\n");
        break;
    }
    system("pause");

    return 0;
}

