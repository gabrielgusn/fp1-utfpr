/* 1) A contribuição para o INSS é calculada da seguinte forma:
a) Salário bruto até três salários mínimos = INSS 8%.
b) Salário bruto acima de três salários mínimos = INSS 10%.
c) Para as contribuições maiores que o salário mínimo, considerar a importância de um salário mínimo.
Elaborar um programa que receba como entrada o valor do salário mínimo e o valor do salário bruto, calcule o 
INSS e o salário líquido restante e informe-os. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float salM, salB, salL, inss;

    printf("Insira o valor do Salario Minimo: R$ ");
    scanf("%f", &salM);
    printf("Insira o valor do Salario Bruto: R$ ");
    scanf("%f", &salB);
   
    if (salB <= (3*salM))
    {
        inss = salB * 0.08;
        salL = salB - inss;
        printf("\nO INSS a ser pago (8%%) eh de R$ %.2f \n", inss);
        printf("O Salario Liquido eh de R$ %.2f\n\n", salL);
    }
    else if(salB>(salM*3))
    {
        inss = salB * 0.10;
        if(inss>salM)
        {
            inss = salM;
        }
        salL = salB - inss;
        printf("\nO INSS a ser pago (10%%) eh de R$ %.2f \n", inss);
        printf("O Salario Liquido eh de R$ %.2f\n\n", salL);
    }
    

    system("pause");

    return 0;
}
