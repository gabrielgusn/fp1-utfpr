/* 9) Criar um programa que leia o peso (em kg) de uma pessoa e calcule e imprima:
a) O peso da pessoa em gramas.
b) O novo peso, em gramas, se a pessoa engordar 12%. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float kg, g, kg12, g12;
    printf("Informe o seu peso(em Kg): ");
    scanf("%f", &kg);

    g = kg * 1000;
    g12 = g + (g * 0.12);
    kg12 = kg + (kg * 0.12);

    printf("Peso em gramas: %.2f \n", g);
    printf("Peso em gramas caso engorde em 12%%: %.2f \n", g12);
    printf("Peso em Kg caso engorde em 12%%: %.2f \n", kg12);

    system("pause");

    return 0;
}
