/* 1) Ler um n�mero double. Separar a parte inteira e a parte
decimal desse n�mero. Apresentar a parte decimal como um valor
double e como um inteiro de tr�s d�gitos. Da parte inteira
separar o n�mero que representa unidade, dezena e centena e
mostrar. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    double num, dec;
    int inte, decInt, cen, dez, un;

    printf("Insira um valor double: ");
    scanf("%lf", &num);

    inte = (int) num;

    dec = num - (double)inte;
    decInt = dec * (int)1000;
    cen = inte / 100;
    dez = (inte%100)/10;
    un = (inte%100)%10;

    printf("N�mero informado: %lf \n", num);
    printf("Parte inteira: %d \n", inte);
    printf("Parte decimal: %lf \n", dec);
    printf("Parte decimal como inteiro de tr�s d�gitos: %d \n", decInt);
    printf("Centena(s): %d \n", cen);
    printf("Dezena(s): %d \n", dez);
    printf("Unidade(s): %d \n", un);

    system("pause");

    return 0;
}
