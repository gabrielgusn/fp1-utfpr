/* 3) Leia um valor double que representa o troco a ser
fornecido por um caixa. Separe a parte inteira (reais) da
parte decimal (centavos) e apresente na forma: 123 reais e
18 centavos. Use a fun��o round, da biblioteca math.h, para
o arredondamento da parte decimal). */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    double troco;
    int partInt;
    float parteDec;

    printf("Informe o valor do troco: R$ ");
    scanf("%lf", &troco);

    partInt = floor(troco);
    parteDec = (troco - partInt)*100;

    printf("O valor informado � %d reais e %.0f centavos \n", partInt, parteDec);

    system("pause");

    return 0;
}
