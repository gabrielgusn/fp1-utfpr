/* 4) Fazer um programa que leia um n�mero inteiro de at�
tr�s d�gitos (considere que ser� fornecido um n�mero de at�
3 d�gitos), calcule e imprima a soma dos seus d�gitos. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    int numInt, a, b, c, d;
    printf("Insira um n�mero inteiro de at� 3 digitos: ");
    scanf("%d", &numInt);

    a = numInt / 100;
    b = (numInt % 100) / 10;
    c = (numInt % 100) % 10;
    d = a + b + c;

    printf ("%d = %d + %d + %d  = %d \n \n", numInt, a, b, c, d);

    system("pause");

    return 0;
}
