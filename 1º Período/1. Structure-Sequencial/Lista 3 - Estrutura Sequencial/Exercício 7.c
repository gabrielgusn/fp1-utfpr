/* 7) Suponha que um caixa disponha apenas de c�dulas de
R$ 100, R$ 10 e R$ 1. Escreva um programa para ler o valor de
uma compra e o valor fornecido pelo usu�rio para pagar essa
compra, e calcule o troco. Calcular e mostrar a quantidade de
cada tipo de c�dula que o caixa deve fornecer como troco.
Mostrar tamb�m o valor da compra e do troco. Use vari�veis do
tipo int. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int compra, pago, trocoTotal, tr1, tr10, tr100;

    printf("Informe o valor da compra: R$ ");
    scanf("%d", &compra);
    printf("Informe o valor pago: R$ ");
    scanf("%d", &pago);

    trocoTotal = pago - compra;
    tr100 = trocoTotal / 100;
    trocoTotal = trocoTotal - (tr100 * 100);

    tr10 = trocoTotal / 10;
    trocoTotal = trocoTotal - (tr10 * 10);
    tr1 = trocoTotal / 1;

    printf("O troco de R$ %d foi pago com: \n", trocoTotal);
    printf("  %d notas de R$ 100 \n", tr100);
    printf("  %d notas de R$ 10 \n", tr10);
    printf("  %d notas de R$ 1 \n \n", tr1);

    system("pause");

    return 0;
}
