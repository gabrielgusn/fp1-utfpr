/* 5) Fa�a um programa para calcular a quantidade de fita
necess�ria, em cent�metros, para amarrar um pacote com duas
voltas, sendo uma pela largura e outra pelo comprimento do
pacote. Ser�o fornecidas a largura, a altura e o comprimento
do pacote. Para amarrar as pontas da fita s�o necess�rios
15 cm de fita em cada ponta */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float larg, alt, comp, fita;

    printf("Informe a largura(em cm): ");
    scanf("%f", &larg);
    printf("Informe a altura(em cm): ");
    scanf("%f", &alt);
    printf("Informe a comprimento(em cm): ");
    scanf("%f", &comp);

    fita = 30 + (alt * 4) + (larg * 2) + (comp * 2);

    printf("Ser�o necess�rios %.0fcm de fita. \n \n", fita);

    system("pause");

    return 0;
}
