/* 8) Ler um n�mero que representa a quantidade de dias.
Informe os anos (considere-os com 360 dias), meses
(considere-os com 30 dias) e os dias contidos nesse valor */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int tDias, a, m, d;

    printf("Informe o tempo em dias: ");
    scanf("%d", &tDias);

    a = tDias / 360;
    m = (tDias % 360)/ 30;
    d = ((tDias % 360) % 30) % 7;

    printf("%d dias equivalem a %d ano(s), %d mes(es) e %d dia(s) \n", tDias, a, m, d);

    system("pause");

    return 0;
}
