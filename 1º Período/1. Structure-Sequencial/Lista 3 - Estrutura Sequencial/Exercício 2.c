/* 2) Considerando que para um cons�rcio sabe-se o n�mero
total de presta��es, a quantidade de presta��es pagas e o
valor de cada presta��o (que � fixo). Escreva um programa
que determine o valor total j� pago pelo consorciado e o
saldo devedor.*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int presTotal, presPag;
    float valFix, valPag, saldDeve;

    printf("Informe a quantidade total de presta��es: ");
    scanf("%d", &presTotal);
    printf("Informe a quantidade de presta��es pagas: ");
    scanf("%d", &presPag);
    printf("Informe o valor (fixo) da presta��o: R$ ");
    scanf("%f", &valFix);

    valPag = presPag*valFix;
    saldDeve = (valFix*presTotal) - valPag;

    printf("Valor total  j� pago: R$ %.2f \n", valPag);
    printf("Saldo devedor: R$ %.2f \n \n", saldDeve);

    system("pause");

    return 0;
}
