/* 6) Ler um n�mero inteiro longo (long int) que representa
os segundos e convert�-lo para horas, minutos e segundos.
Mostrar a quantidade de horas, minutos e segundos obtidos, no
seguinte formato: xhoras:yminutos:zsegundos. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    long int x, y, z, tempo;

    printf("Informe um n�mero inteiro longo: ");
    scanf("%li", &tempo);

    x = tempo / 3600;
    y = (tempo % 3600) / 60;
    z = (tempo % 3600) % 60;

    if (x != 1)
        {
        printf ("%.li horas: %.li minutos: %.li segundos \n \n", x, y,z);
        }

    if (x == 1)
        {
        printf ("%.li hora: %.li minutos: %.li segundos \n \n", x, y,z);
        }

    system("pause");

    return 0;
}
