/* 4) Escreva um programa que, dados a quantidade de litros de
combust�vel utilizada, os quil�metros percorridos por um
autom�vel e o valor do litro de combust�vel, calcule quantos
quil�metros o ve�culo
percorreu por litro de combust�vel e o valor gasto em reais
por km. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    float km, gas, vl_Gas, media, gastoKm;

    printf("Informe os km percorridos: ");
    scanf("%f", &km);
    printf("Informe o combust�vel consumido(em litros): ");
    scanf("%f", &gas);
    printf("Informe o valor do litro de combust�vel: R$ ");
    scanf("%f", &vl_Gas);

    media = km/gas;
    gastoKm = (gas/km)*vl_Gas;

    printf("O autom�vel fez %.2f km por litro de combust�vel \n", media);
    printf("O gasto em reais por km foi de R$ %.2f \n", gastoKm);

    system("pause");
    return 0;
}
