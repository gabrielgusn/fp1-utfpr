/* 9) Escreva um programa que o leia o n�mero de horas trabalhadas por um funcion�rio, o valor por hora, o
n�mero de filhos com idade menor do que 14 anos, o valor do sal�rio fam�lia por filho e calcule e mostre o
sal�rio desse funcion�rio. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float valHora, salFam, salTotal;
    int horas, filhos;

    printf("Informe o n�mero de horas trabalhadas: ");
    scanf("%d", &horas);
    printf("Informe o valor da hora trabalhada: R$ ");
    scanf("%f", &valHora);
    printf("Informe o n�mero de filhos menores de 14 anos: ");
    scanf("%d", &filhos);
    printf("Informe o valor do sal�rio da fam�lia: R$ ");
    scanf("%f", &salFam);

    salTotal = (horas*valHora) + (filhos*salFam);

    printf("Sal�rio: R$ %.2f \n \n", salTotal);

    system("pause");

    return 0;
}
