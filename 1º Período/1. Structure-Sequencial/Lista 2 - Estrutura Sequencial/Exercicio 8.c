/* 8) Fa�a um programa que leia o sal�rio bruto mensal de um
funcion�rio, calcule e mostre os valores conforme o exemplo
a seguir. Observa��o: � poss�vel fazer esse programa
utilizando somente tr�s vari�veis: uma para ler o sal�rio
bruto, outra para os descontos e outra para o sal�rio
l�quido. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float salBruto, convert, desc, salLiq;

    printf("Informe o Sal�rio Bruto: R$ ");
    scanf("%f", &salBruto);

    desc = salBruto * 0.15;
    printf("(-) IR (15%%): R$ %.2f \n", desc);

    desc = salBruto * 0.11;
    printf("(-) INSS (11%%): R$ %.2f \n", desc);

    desc = salBruto * 0.03;
    printf("(-) Sindicado (3%%): R$ %.2f \n", desc);

    salLiq = salBruto - (salBruto * (0.15+0.11+0.03));
    printf("Sal�rio L�quido: R$ %.2f \n \n", salLiq);

    system("pause");

    return 0;
}
