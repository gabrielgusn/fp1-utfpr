/* 7) Um viajante de carro far� o trajeto entre duas cidades e
 ao t�rmino da viagem deseja saber:
a) Quantas vezes foi necess�rio abastecer o carro. (Use a
fun��o ceil() da biblioteca math.h para arredondar o valor
para cima)
b) Quantos litros foram consumidos para percorrer a dist�ncia
indicada.
c) Quantos litros restaram no tanque ap�s a chegada ao
destino.
Fa�a um programa que leia a dist�ncia entre as duas cidades,
a capacidade do tanque e o consumo m�dio do ve�culo, calcule
e mostre as informa��es solicitadas. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float km, tanque, consumo, trajeto, abast, rest;


    printf("Informe a dist�ncia(em km) percorrida: ");
    scanf("%f", &km);
    printf("Informe a capacidade m�x. do tanque de combust�vel: ");
    scanf("%f", &tanque);
    printf("Informe o consumo m�dio (em km//l): ");
    scanf("%f", &consumo);

    trajeto = km/consumo;
    abast = ceil(trajeto/tanque);
    rest = (abast*tanque)-trajeto;

    printf("Foram necess�rios %.1f litros de combust�vel para percorrer o trajeto. \n", trajeto);
    printf("Foi necess�rio abastecer o ve�culo %.0f vezes. \n", abast);
    printf("Restou %.1f litros no tanque de combust�vel. \n", rest);

    system("pause");

    return 0;
}
