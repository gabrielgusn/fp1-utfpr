/* 5) Escreva um programa para ler o n�mero de votos brancos,
nulos (incluem eleitores ausentes) e v�lidos de uma elei��o.
Calcular e mostrar o percentual que cada um (brancos, nulos e
v�lidos) representa em rela��o ao total de eleitores. Lembrar
que os valores dos percentuais podem n�o ser inteiros. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    int branco, nulo, valido;
    float total1, total2;

    printf("Informe o n�mero de votos v�lidos: ");
    scanf("%d", &valido);
    printf("Informe o n�mero de votos em branco: ");
    scanf("%d", &branco);
    printf("Informe o n�mero de votos nulos: ");
    scanf("%d", &nulo);

    total1 = valido+branco+nulo;
    //v�lidos
    total2 = (100*valido)/total1;
    printf("N�mero de votos v�lidos: %.1f%% \n", total2);

    //em branco
    total2 = (100*branco)/total1;
    printf("N�mero de votos em branco: %.1f%% \n", total2);

    //nulos
    total2 = (100*nulo)/total1;
    printf("N�mero de votos nulos: %.1f%% \n", total2);

    system("pause");
    return 0;
}
