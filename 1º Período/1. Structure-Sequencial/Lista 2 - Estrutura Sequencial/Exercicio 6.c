/* 6) Fa�a um programa que leia o pre�o de uma mercadoria com
diferen�a de um m�s (ler o valor da mercadoria no m�s passado
e no m�s atual) e calcule a taxa de infla��o mensal dessa
mercadoria. A infla��o � o percentual da diferen�a de pre�os
(atual menos o anterior) sobre o pre�o anterior */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    float vl_Ant, vl_At, infl;

    printf("Informe o pre�o anterior: ");
    scanf("%f", &vl_Ant);
    printf("Informe o pre�o atual: ");
    scanf("%f", &vl_At);

    /* 100 = 100
        120 = x   100x = 120x100
        x =12000/100*/
    infl = ((100*vl_At)/vl_Ant)-100;

    printf("A taxa de infla��o � de: %.2f%% \n", infl);

    system("pause");
    return 0;
}
