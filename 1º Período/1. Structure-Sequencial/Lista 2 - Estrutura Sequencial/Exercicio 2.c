/* 2) O custo ao consumidor de um carro novo � a soma do custo
de f�brica com a percentagem do distribuidor e a percentagem
dos impostos (ambas aplicadas sobre o custo de f�brica).
Escrever um programa para, a partir do custo de f�brica do
carro, calcular e mostrar o custo ao consumidor. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    float custoF, percD, imposto, custoC;

    printf("C�lculo de Custo ao Consumidor \n \n");
    printf("Informe o custo de f�brica do autom�vel: R$ ");
    scanf("%f", &custoF);
    printf("Informe a %% do distribuidor(0 a 100): ");
    scanf("%f", &percD);
    printf("Informe a %% de impostos(0 a 100): ");
    scanf("%f", &imposto);

    custoC = custoF + (custoF * (percD/100)) + (custoF *(imposto/100));

    printf("O custo do ve�culo ao consumidor �: R$ %.2f \n", custoC);

    system("pause");

    return 0;
}
