/* 3) Escreva um programa que calcule o valor da convers�o para
d�lares de um valor lido em reais. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float dolar, real, vlConvert;

    printf("Informe o valor atual do d�lar: U$ ");
    scanf("%f", &dolar);
    printf("Informe o valor em reais: R$ ");
    scanf("%f", &real);

    vlConvert = real/dolar;

    printf("R$ %f equivalem a U$ %.2f \n", real, vlConvert);

    system("pause");
    return 0;
}
