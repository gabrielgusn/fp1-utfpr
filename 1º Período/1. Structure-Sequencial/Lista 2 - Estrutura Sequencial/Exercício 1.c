/* 1) Ler dois n�meros float e apresentar, sem utilizar
fun��es matem�ticas prontas:
    a) A divis�o do primeiro n�mero pelo segundo, armazenando
    somente a parte inteira do n�mero.
    b) A soma dos dois n�meros com o resultado arredondado
    para o pr�ximo n�mero inteiro.
Obs.: As vari�veis de resultados devem ser do tipo int. */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    float num1, num2;
    int a, b;

    printf("Informe um valor float: ");
    scanf("%f", &num1);
    printf("Informe outro valor float: ");
    scanf("%f", &num2);

    a = num1 / num2;
    b = num1 + num2 + 0.99999;

    printf("%f / %f = %d \n", num1, num2, a);
    printf("%f + %f = %i \n", num1, num2, b);

    system("pause");

    return 0;
}
