/* Elaborar um algoritmo que calcule e mostre a media de
idade de 3 pessoas */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int age1, age2, age3;
    float media;

    printf("Insira a Idade 1: ");
    scanf("%d", &age1);
    printf("Insira a Idade 2: ");
    scanf("%d", &age2);
    printf("Insira a Idade 3: ");
    scanf("%d", &age3);

    media = (age1+age2+age3)/3;

    printf("A media de idade equivale a: %.2f anos \n", media);

    system("pause");

    return 0;

}
