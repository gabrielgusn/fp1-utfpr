/* Elaborar um algoritmo que receba as notas dos alunos e calcule a m�dia ponderada
aplicando a cada uma seu respectivo peso (1, 2 e 3, respectivamente)*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (void)
{
    float  nota1, nota2, nota3, mP; //mP = M�dia Ponderada

    printf("Insira a Nota 1: ");
    scanf("%f", &nota1);
    printf("Insira a Nota 2: ");
    scanf("%f", &nota2);
    printf("Insira a Nota 3: ");
    scanf("%f", &nota3);

    mP = (nota1*1 + nota2*2 + nota3*3)/(1+2+3);

    printf("Media Final: %.2f", mP);

    system("pause");

    return 0;

}
