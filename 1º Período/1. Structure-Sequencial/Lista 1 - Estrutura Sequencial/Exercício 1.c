/* Criar um algoritmo que leia o comprimento(cm), largura(cm), e altura(cm)
de um bloco retangular e, ap�s isso, calcule o volume do objeto*/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main (void)
{
    //Declara��o de Vari�veis
    float comp, larg, alt, vol;

    //Entrada de Dados
    printf("Insira o Comprimento do Bloco Retangular(em cm): ");
    scanf("%f", &comp);
    printf("Insira a Largura do Bloco Retangular(em cm): ");
    scanf("%f", &larg);
    printf("Insira a Altura do Bloco Retangular(em cm): ");
    scanf("%f", &alt);

    //Processamento de Dados
    vol = comp * larg * alt;

    //Sa�da de Dados
    printf("Volume do Bloco Retangular: %.2f \n", vol);

    system("pause");

    return 0;

}
