/* Criar um algoritmo que leia o raio de um c�rculo e informe sua circunfer�ncia */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    float r, C; //r= raio, C= Circunfer�ncia

    printf("Insira o Raio da Circunferencia: ");
    scanf("%f", &r);

    C = 3.1415 * r * 2;

    printf("A Circunferencia �: %.2f", C);

    system("pause");

    return 0;
}
