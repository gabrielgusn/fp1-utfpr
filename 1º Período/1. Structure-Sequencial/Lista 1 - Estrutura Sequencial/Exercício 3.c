/* Criar um algoritmo que leia o valor de uma presta��o e da taxa de juros cobrada pelo atraso da mesma */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    float prest, juros, montante;

    printf("Declare o Valor da Parcela Atrasada: R$ ");
    scanf("%f", &prest);
    printf("Declare a Taxa de Juros a ser aplicada: %");
    scanf("%f", &juros);

    montante = prest + (prest * juros /100);

    printf("Total a ser pago: R$ %.2f \n", montante);

    system("pause");

    return 0;
}
