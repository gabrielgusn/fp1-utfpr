/* Criar algoritmo capaz de calcular a tabuada de 0 a 10 de um valor int
digitado pelo usu�rio */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
        int num, z, u, d, t, q, c, s, st, o, n, dez;

        printf("Informe um numero inteiro: ");
        scanf("%d", &num);

        z = num * 0;
        u = num * 1;
        d = num * 2;
        t = num * 3;
        q = num * 4;
        c = num * 5;
        s = num * 6;
        st = num * 7;
        o = num * 8;
        n = num * 9;
        dez = num * 10;

        printf("%d * 0= %d \n", num, z);
        printf("%d * 1= %d \n", num, u);
        printf("%d * 2= %d \n", num, d);
        printf("%d * 3= %d \n", num, t);
        printf("%d * 4= %d \n", num, q);
        printf("%d * 5= %d \n", num, c);
        printf("%d * 6= %d \n", num, s);
        printf("%d * 7= %d \n", num, st);
        printf("%d * 8= %d \n", num, o);
        printf("%d * 9= %d \n", num, n);
        printf("%d * 10= %d \n", num, dez);

        system("pause");

        return 0;
}
