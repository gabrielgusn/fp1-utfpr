/* Elabora��o de um algoritmo respons�vel por calcular e
mostrar na tela a soma, subtra��o do primeiro pelo segundo,
multiplica��o entre eles, divis�o int do 1o pelo 2o, div float
do 1o pelo 2o, resto da divis�o do primeiro pelo segundo */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    int value1, value2, soma, sub1p2, mult, divint1p2;
    float divfloat1p2, restDiv1p2;

    printf("Insira um valor inteiro: ");
    scanf("%d", &value1);
    printf("Insira outro valor inteiro: ");
    scanf("%d", &value2);

    soma = value1 + value2;
    sub1p2 = value1 - value2;
    mult = value1 * value2;
    divint1p2 = value1 / value2;
    divfloat1p2 = value1 / value2;
    restDiv1p2 = value1 % value2;

    printf("Soma: %d \n", soma);
    printf("Subtracao do 1o pelo 2o: %d \n", sub1p2);
    printf("Produto: %d \n", mult);
    printf("Quociente inteiro do 1o pelo 2o: %d \n", divint1p2);
    printf("Quociente float do 1o pelo 2o: %.2f \n", divfloat1p2);
    printf("Resto 1o pelo 2o: %.2f \n", restDiv1p2);

    system("pause");

    return 0;
}
