/* Elaborar um algoritmo que calcule o salario apos aplicacao
de percentual de reajuste salarial */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    float salAtual, salReaj, percReaj;

    printf("Insira o valor do salario: R$ ");
    scanf("%f", &salAtual);
    printf("Insira o percentual de reajuste: ");
    scanf("%f", &percReaj);

    salReaj = (salAtual*(percReaj/100))+salAtual;

    printf("Salario apos reajuste: R$ %.2f \n", salReaj);

    system("pause");

    return 0;
}
