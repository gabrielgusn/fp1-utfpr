/* Elaborar algoritmo respons�vel por calcular o valor do sal�rio l�quido de uma pessoa
ap�s serem descontados imposto de renda e INSS*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    float salB, inss, ir, salL;

    printf("Informe o Salario Bruto: R$ ");
    scanf("%f", &salB);
    printf("Informe o percentual de INSS: ");
    scanf("%f", &inss);
    printf("Informe o percentual de IR: ");
    scanf("%f", &ir);

    salL = salB - (salB *(inss + ir)/100);

    printf("Salario Liquido: %.2f \n", salL);

    system("pause");

    return 0;

}
