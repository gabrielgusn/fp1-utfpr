/* Elabora��o de algoritmo que fa�a a convers�o de temperatura em Celsius para Fahrenheit */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    float tempC, tempF;

    printf("Temperatura em Celsius: ");
    scanf("%f", &tempC);

    tempF = (1.8 * tempC) + 32;

    printf("Temperatura convertida em Fahrenheit: %.1f \n", tempF);

    system("pause");

    return 0;
}
