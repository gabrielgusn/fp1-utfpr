/* Elaborar um algoritmo que ap�s inseridos os valor de entrada
A e B, realize a troca do conte�do das vari�veis */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int vA;
    int vB;
    int vC;

    printf("Insira um Valor A: ");
    scanf("%d", &vA);
    printf("Insira um Valor B: ");
    scanf("%d", &vB);

    printf("Antes da Troca: A= %d e B= %d \n", vA, vB);

    vC = vA;
    vA = vB;
    vB = vC;

    printf("Depois da Troca: A= %d e B= %d \n",vA, vB);

    system("pause");

    return 0;
}
