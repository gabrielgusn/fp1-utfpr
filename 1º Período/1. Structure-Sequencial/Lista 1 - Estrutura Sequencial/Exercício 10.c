/* Elaborar um algoritmo respons�vel por resolver express�es
matem�ticas dados os valores de X e Y pelo usu�rio */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    int x, y, resultadoInt;
    float resultado;

    printf("Insira o Valor de X: ");
    scanf("%d", &x);
    printf("Insira o Valor de Y: ");
    scanf("%d", &y);

    resultado = (((float)x + y) / y) * (pow(x, 2));
    printf("a) %5.2f \n", resultado);

    resultado = ((float)x+y) / (x-y);
    printf("b) %5.2f \n", resultado);

    resultado = ((pow(x,2)) + (pow(y,3))) / 2.0;
    printf("c) %5.2f \n", resultado);

    resultado = pow((float)x, 3)/pow(x, 2);
    printf("d) %5.2f \n", resultado);

    resultadoInt = x % y;
    printf("e) %5d \n", resultadoInt);

    resultadoInt = x % 3;
    printf(" %7d \n", resultadoInt);

    resultadoInt = y % 5;
    printf(" %7d \n", resultadoInt);



    system("pause");
    return 0;
}
