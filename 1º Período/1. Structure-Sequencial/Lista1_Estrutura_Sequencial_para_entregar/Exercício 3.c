/*3) Cada degrau de uma escada tem uma altura X. Fa�a um programa para ler essa altura e a altura que o
usu�rio deseja alcan�ar subindo a escada. Calcule e mostre quantos degraus o usu�rio dever� subir para
atingir o seu objetivo */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    int degrau;
    float altEscada, altDegrau;

    printf("Informe a altura da Escada (em cm): ");
    scanf("%f", &altEscada);
    printf("Informe a altura do Degrau (em cm): ");
    scanf("%f", &altDegrau);

    degrau = ceil(altEscada/altDegrau);

    printf("Voce devera subir %d degraus para alcancar a altura desejada \n", degrau);

    system("pause");

    return 0;
}
