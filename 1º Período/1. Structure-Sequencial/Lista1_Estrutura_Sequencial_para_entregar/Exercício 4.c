/* 4) Fa�a um programa que receba o peso e a altura de uma
pessoa e calcule o �ndice de massa corp�rea. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    float peso, alt_m, imc;

    printf("CALCULADORA DE IMC \n \n \n");
    printf("Insira a sua altura(em m): ");
    scanf("%f", &alt_m);
    printf("Insira seu peso(em kg): ");
    scanf("%f", &peso);

    imc = peso/(pow(alt_m,2));

    printf("Seu IMC eh: %.2f \n", imc);


//Queria testar a fun��o if
    if(imc<18)
    {
        printf("Seu peso esta abaixo do ideal \n");
        return 0;
    }

    if(imc>30)
    {
        printf("Seu peso esta acima do ideal \n");
        return 0;
    }
    if(18<imc<30)
    {
        printf("Seu peso esta no nivel ideal \n");
    }
    system("pause");

    return 0;

}
