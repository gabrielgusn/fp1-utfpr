/* 5) Um hotel deseja fazer uma promo��o especial de final de
semana, concedendo um desconto de 25% na di�ria. Sendo
informados o n�mero de apartamentos do hotel e o valor da
di�ria por apartamento para o final
de semana completo. Elaborar um programa para calcular:
a) Valor promocional da di�ria;
b) Valor total caso a ocupa��o no final de semana atinja 100%;
c) Valor total a ser arrecadado caso a ocupa��o no final de
semana atinja 70%;
d) Valor que o hotel deixar� de arrecadar em virtude da
promo��o, caso a ocupa��o atinja 100%. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    int n_aps;
    float vl_diaria, vl_promo, total;

    printf("Informe o valor da diaria: R$ ");
    scanf("%f", &vl_diaria);
    printf("Informe a quantidade de aps: ");
    scanf("%d", &n_aps);

    vl_promo = vl_diaria-(vl_diaria*0.25);
    printf("Valor da diaria promocional: R$ %.2f \n", vl_promo);

    //total 100% de ocupa��o
    total = 2*(vl_promo * n_aps);
    printf("Total arrecadado com 100%% de ocupacao: R$ %.2f \n", total);
    //total com 70% de ocupa��o
    total = 2*vl_promo * (n_aps*0,7);
    printf("Total arrecadado com 70%% de ocupacao: R$ %.2f \n", total);
    //deixara de arrecadar
    total = 2*((vl_diaria * n_aps) - (vl_promo * n_aps));
    printf("Deixara de arrecadar R$ %.2f caso atinja 100%% de ocupacao ao aplicar o desconto \n", total);

    system("pause");

    return 0;

}
