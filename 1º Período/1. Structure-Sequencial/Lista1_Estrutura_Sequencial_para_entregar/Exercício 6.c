/* 6) Uma pessoa resolveu fazer uma aplica��o em uma poupan�a programada1
. Para calcular seu rendimento,
ela dever� fornecer o valor constante da aplica��o mensal, a taxa de juros (%) e o prazo da aplica��o (em
meses).  */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    int prazo;
    float juros, vl_aplic, vl_acum;

    printf("Informe o valor da aplicacao: R$ ");
    scanf("%f", &vl_aplic);
    printf("Informe a taxa de rendimento (em %%): ");
    scanf("%f", &juros);
    printf("Informe o numero de meses: ");
    scanf("%d", &prazo);

    juros = juros/100;

    vl_acum = vl_aplic*(pow(1+juros, prazo)-1)/juros;

    printf("o Valor Final apos %d meses de aplicacao eh de R$ %.2f \n", prazo, vl_acum);
    system("pause");

    return 0;

}
