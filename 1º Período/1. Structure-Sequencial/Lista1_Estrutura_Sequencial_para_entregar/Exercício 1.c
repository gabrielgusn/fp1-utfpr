/* Fa�a um programa que receba o custo de um espet�culo
teatral e o pre�o do convite desse espet�culo.
Esse programa deve calcular e mostrar:
a) A quantidade de convites que devem ser vendidos para
cobrir o custo do espet�culo.
b) A quantidade de convites que devem ser vendidos para
cobrir o custo do espet�culo e ainda obter um lucro
de 25%. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    float custo, ing;
    int venda, lucro;

    printf("Informe o valor de custo do espetaculo: ");
    scanf("%f", &custo);
    printf("Informe o valor do ingresso: ");
    scanf("%f", &ing);

    venda = ceil(custo/ing);
    lucro = ceil(venda+(venda*0.25));

    printf("Sera necessario vender: %d ingressos para cobrir o custo do espetaculo \n", venda);
    printf("Para ter lucro de 25%%, sera necessario vender %d ingressos \n", lucro);

    system("pause");
    return 0;
}
