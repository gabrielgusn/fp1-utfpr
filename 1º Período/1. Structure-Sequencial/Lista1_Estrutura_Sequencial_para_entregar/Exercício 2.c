/* 2) Elabore um programa que leia via teclado uma quantidade
de segundos (tipo int) e transforme este tempo em dias, horas
e minutos (as tr�s �ltimas em tipo float).*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int seg;
    float dias, horas, min;

    printf("Informe o tempo em segundos: ");
    scanf("%d", &seg);

    min = seg/60;
    horas = min/60;
    dias = horas/24;

    printf("O tempo em minutos eh: %.2f \n", min);
    printf("O tempo em horas eh: %.2f \n", horas);
    printf("O tempo em dias eh: %.2f \n", dias);

    system("pause");

    return 0;
}
