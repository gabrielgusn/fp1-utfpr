/* 1) Fazer um programa que leia um valor double que
representa o sal�rio de uma pessoa. Apresente separadamente
os reais (parte inteira) e os centavos (parte decimal). */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    double sal;
    int partInt;
    float parteDec;

    printf("Informe o valor do sal�rio: R$ ");
    scanf("%lf", &sal);

    partInt = floor(sal);
    parteDec = (sal - partInt)*100;

    printf("Sal�rio Informado: R$ %.2f \n", sal);
    printf("Reais: %d \n", partInt);
    printf("Centavos: %.0f \n", parteDec);

    system("pause");

    return 0;
}
