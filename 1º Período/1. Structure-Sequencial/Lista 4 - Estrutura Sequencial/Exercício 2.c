/* 2) Fazer um programa para ler o sal�rio de uma pessoa, o percentual de aumento e o percentual de descontos.
Os descontos incidem sobre o sal�rio com aumento. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    float sal, aumento, desconto, salBruto, salLiq, salDec;
    int salInt;

    printf("Informe o valor do sal�rio: R$ ");
    scanf("%f", &sal);
    printf("Informe o percentual de aumento: ");
    scanf("%f", &aumento);
    printf("Informe o percentual de desconto: ");
    scanf("%f", &desconto);

    salBruto = sal + (sal*(aumento/100));
    salLiq = salBruto - (salBruto*(desconto/100));

    printf("Sal�rio aumentado: R$ %.2f \n", salBruto);
    printf("Sal�rio L�quido: R$ %.2f \n", salLiq);

    salInt = floor(salLiq);
    salDec = (salLiq - salInt)*100;

    printf("O sal�rio l�quido � %d reais e %.0f centavos \n \n", salInt, salDec);

    system("pause");

    return 0;
}
