/* 2) Ler um n�mero positivo, validar a entrada repetindo a leitura at� que seja informado um n�mero que
atende essa condi��o. Esse n�mero representa a quantidade de n�meros primos a serem mostrados.*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{

    char rep;

    do//repeticao do programa
    {
       int contDiv=0, num=0, contPrint=0;
       do
       {
         printf("Informe a quantia de primos a ser mostrados: ");
         scanf("%d", &num);
         if(num<0)
         {
             printf("O numero deve ser positivo\n");
         }
       }while(num<0);
        int i=2;
        do
        {
            contDiv=0;
            for(int j=0; j<=i; j++)
            {
                if(i%j==0)
                {
                    contDiv++;
                }
            }
                if(contDiv=2)
                {
                    printf("%d \t",i);
                    contPrint++;
                }
               i++;
        }while(contPrint<num);
       do//validacao sim ou nao
       {
           printf("\nDeseja executar novamente? (S ou N)\n");
           setbuf(stdin,NULL);
           scanf("%c", &rep);
           rep=toupper(rep);
           if(rep != 'S' && rep != 'N')
           {
               printf("Caractere Invalido\n\n");
           }
       }while(rep != 'S' && rep != 'N');

    }while(rep=='S');

    system("pause");
    return 0;
}
