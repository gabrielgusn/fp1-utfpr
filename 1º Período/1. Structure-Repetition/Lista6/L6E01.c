/* 1) Ler um n�mero positivo, validar a entrada repetindo a leitura at� que seja informado um n�mero que
atende essa condi��o. Esse n�mero representa a quantidade de n�meros �mpares a serem mostrados.
Apresentar esses valores n por linha, sendo n um n�mero maior que zero, informado pelo usu�rio. Os valores
s�o apresentados separados por tabula��o. Fazer a m�dia dos n�meros �mpares mostrados. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{

    char rep;

    do//repeticao do programa
    {
       int contImp=0, num=0, n=0, soma=0, contPrint;
       do
       {
         printf("Informe um numero: ");
         scanf("%d", &num);
         if(num<0)
         {
             printf("O numero deve ser positivo\n");
         }
       }while(num<0);

       do
       {
         printf("Informe o numero de valores por linha: ");
         scanf("%d", &n);
         if(n<1)
         {
             printf("O numero de valores por linha deve ser maior que 0\n");
         }
       }while(n<1);

        for(int i=0; i<=num;i++)
        {
            if(i%2!=0)
            {
                printf("%d\t", i);
                contImp++;
                contPrint++;
                soma = soma+i;
            }
            if(contPrint==n)
            {
                printf("\n");
                contPrint=0;
            }
        }
        printf("\nExistem %d impares entre 0 e %d\n", contImp, num);
        printf("A media dos impares eh: %.2f\n", (float)soma/contImp);

       do//validacao sim ou nao
       {
           printf("\nDeseja executar novamente? (S ou N)\n");
           setbuf(stdin,NULL);
           scanf("%c", &rep);
           rep=toupper(rep);
           if(rep != 'S' && rep != 'N')
           {
               printf("Caractere Invalido\n\n");
           }
       }while(rep != 'S' && rep != 'N');

    }while(rep=='S');

    system("pause");
    return 0;
}
