/* FOR
6) Ler dois números que representam os limites de um intervalo e ler o incremento. Mostrar os números desse 
intervalo de acordo com o incremento indicado e em ordem crescente. O usuário pode informar os valores 
que representam os limites do intervalo em ordem crescente ou decrescente. Fazer a média dos ímpares e 
divisíveis por 35 do intervalo.*/ 
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num1, num2, soma=0, contMed=0;

    printf("Informe o valor 1: ");
    scanf("%d", &num1);
    printf("Informe o valor 2: ");
    scanf("%d", &num2);
    if(num1>num2)
    {
        for(num2; num2<num1; num2++)
        {
            printf("%d\n", num2);
            if(num2==num1-1)
            {
                printf("\n");
            }
            if(num2%2!=0 && num2%35==0)
            {
                contMed++;
                soma=soma+num2;
            }
        }
    }
    else
    {
        for(num1; num1<num2; num1++)
        {
            printf("%d\n", num1);
            if(num1==num2-1)
            {
                printf("\n");
            }
            if(num1%2!=0 && num1%35==0)
            {
                contMed++;
                soma=soma+num1;
            }
        }
    }
    printf("Media dos Impares Divisiveis por 35: %.2f\n\n", (float)soma/contMed);
    system("pause");
    return 0;
}