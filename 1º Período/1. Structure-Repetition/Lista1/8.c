/* WHILE
8) Fazer um programa que faça o levantamento dos candidatos que se inscreveram para vagas em uma
empresa. Considerando que para cada candidato, a empresa tenha obtido as seguintes informações:
- Idade
- Nacionalidade (B - Brasileiro ou E - Estrangeiro)
- Possui curso superior (S - Sim ou N - Não)
Determinar:
a) A quantidade de brasileiros.
b) A quantidade de estrangeiros.
c) A idade média dos brasileiros sem curso superior.
d) A menor idade entre os estrangeiros com curso superior.
Estabelecer uma condição para finalizar a entrada de dados do programa. Por exemplo, igual a 0 ou idade
negativa. Se a idade for igual a 0 ou negativa, não ler as informações de nacionalidade e se possui curso
superior, e sair do programa. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    int idade=1, somaIdade=0, contBr=0, contCurso=0, contE=0, menor, prim=0;
    char nac, curso;

    while(idade>0)
    {
        printf("Informe a idade: ");
        scanf("%d", &idade);
        if(idade>0)
        {
                do
            {
                printf("Informe a Nacionalidade (B - Brasileiro ou E- Estrangeiro): ");
                setbuf(stdin, NULL);
                scanf("%c", &nac);
                nac=toupper(nac);

                if(nac == 'B')
                {
                    contBr++;
                    setbuf(stdin, NULL);
                }
                else if(nac == 'E')
                {
                    contE++;
                    setbuf(stdin, NULL);
                }
                else
                {
                    printf("Caractere Invalido\n");
                    setbuf(stdin, NULL);
                }

            }while(nac != 'B' && nac != 'E');
            do
            {
                setbuf(stdin,NULL);
                printf("Possui curso superior(S ou N)?\n");
                scanf("%c", &curso);
                curso = toupper(curso);
            }while(curso != 'S' && curso != 'N');

            if(nac == 'B' && curso == 'N')
            {
                somaIdade = somaIdade + idade;
                contCurso++;
            }
            if(nac == 'E' && curso == 'S')
            {
               if(prim ==0)
                {
                    menor=idade;
                    prim=1;
                }
                else
                {
                    if(idade<menor);
                    {
                        menor=idade;
                    }
                }
            }
        }
    }
    printf("Quantidade de Brasileiros: %d\n", contBr);
    printf("Quantidade de Estrangeiros: %d\n", contE);
    if(contCurso>0)
    {
       printf("Idade Media dos brasileiros sem curso superior: %.2f\n", (float)somaIdade/contCurso);
    }
    else
    {
        printf("Nenhum brasileiro sem curso superior cadastrado \n");
    }
    if(contE>0)
    {
        printf("Menor idade de estrangeiro com curso superior: %d\n", menor);
    }
    else
    {
        printf("Nao foram registrados estrangeiros, portanto nao ha menor idade.\n");
    }
    system("pause");
    return 0;
}
