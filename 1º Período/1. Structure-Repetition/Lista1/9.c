/*DO - WHILE
9) Ler um número que indica a quantidade de ímpares iniciando em 1 que deve ser mostrada. O valor 
informado para a quantidade deve ser maior que 0. Validar a entrada. */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;

    do
    {
        printf("Quantos numeros impares quer mostrar? ");
        scanf("%d", &num);
        
        if(num<1)
        {
            printf("O valor deve ser maior que 0\n\nInforme Novamente\n\n");
        }
        else
        {
            for(int i=1; i<=(num*2); i++)
            {
                if(i%2!=0)
                {
                    printf("%d\n", i);
                }
            }
        }  
    }while(num<1);
    
}