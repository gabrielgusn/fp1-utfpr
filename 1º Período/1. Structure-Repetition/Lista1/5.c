/* DO - WHILE e FOR
5) Ler um número maior que 2 e imprimir todos os pares entre 2 e o número lido. Imprimir a soma dos pares, 
o produto dos ímpares que são divisíveis por 9 e a média de todos os números do intervalo.*/
#include <stdio.h>
#include <Stdlib.h>

int main()
{
    int num, soma, prod, contMed;

    do
    {
        prod=1;
        soma=0;
        printf("Insira um numero: ");
        scanf("%d", &num);
        for(int i=2; i<num; i++)
        {
            if(i%2==0)
            {
                printf("%d\t", i);
                soma = soma + i;
                contMed++;
            }
            if(i%9==0)
            {
                prod = prod * i;
            }
            if(i==(num-1))
            {
                printf("\n");
            }
        }
        printf("Soma: %d\n", soma);
        printf("Produto: %d\n", prod);
        printf("Media: %d\n", soma/contMed);
        setbuf(stdin, NULL);
    }while(num>2);
   
    system("pause");
    return 0;
}