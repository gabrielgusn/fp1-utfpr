/* WHILE
7) Ler uma série de números indicados pelo usuário até ser informado o valor zero. Encontrar e mostrar o 
maior e o menor dos valores informados pelo usuário. O valor zero indica o final da leitura e não deve ser 
considerado. */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num=1, teste=0, menor=0, maior=0;

    while(num!=0)
    {
        printf("Informe um numero: ");
        scanf("%d", &num);
        if(num!=0)
        {
            if(teste == 0)
            {
                maior = num;
                menor = num;
                teste = 1;
            }
            else
            {
                if(num > maior)
                {
                    maior = num;
                }
                else if(num < menor)
                {
                    menor = num;
                }
            }
        }
    }
    printf("Maior: %d \t\t Menor: %d\n\n", maior, menor);    system("pause");
    return 0;
}