/* FOR
3) Apresentar os números entre 0 e 4, com intervalo de 0.25 entre eles, separados por tabulação. */ 

#include <stdlib.h>
#include <stdio.h>

int main(void)
{

    for(int i=0; i<=(4/0.25); i++)
    {
        float num=(i *0.25 );
        printf("%.2f\t", num);
    }

    system("pause");
    return 0;
}