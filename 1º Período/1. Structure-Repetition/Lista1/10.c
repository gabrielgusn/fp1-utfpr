/* 10) Uma indústria fabrica roupas categorizadas em masculinas, femininas e infantis. Ler a quantidade e a
respectiva categoria (M, F ou I). Após o loop, calcular e mostrar o percentual de produtos por categoria.
Validar para que seja informada uma categoria válida. Finalizar a leitura quando informado um valor 0 ou
negativo para a quantidade. Ler inicialmente a quantidade e depois a categoria. Se informada uma
quantidade negativa, não ler a categoria. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main()
{
    int qtd, femPerc=0, mascPerc=0, infPerc=0, total;
    char cat;

    do
    {
        printf("Informe a quantidade: ");
        scanf("%d", &qtd);
        if(qtd>0)
        {
            do{
                printf("Informe a categoria - Feminino(F), Masculino(M) ou Infantil(I):");
                setbuf(stdin,NULL);
                scanf("%c", &cat);
                cat=toupper(cat);

                if(cat == 'F')
                {
                    femPerc=femPerc+qtd;
                }
                else if(cat == 'M')
                {
                    mascPerc=mascPerc+qtd;
                }
                else if(cat == 'I')
                {
                    infPerc=infPerc+qtd;
                }
                else
                {
                    printf("Categoria Invalida\n");
                }
            }while(cat!='F' && cat!='M' && cat!='I');
        }
    }while(qtd>0);
    total = femPerc+mascPerc+infPerc;
    printf("%.1f feminino\n", (float)(100*femPerc)/total);
    printf("%.1f masculino\n", (float)(100*mascPerc)/total);
    printf("%.1f infantil\n", (float)(100*infPerc)/total);
    return 0;
}
