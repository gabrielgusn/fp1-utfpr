/* FOR
2) Apresentar os múltiplos de 5, entre 5 e 50 separados por tabulação.*/ 

#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    for(int i=1; i<=10; i++)
    {
        int num=(5*i);
        printf("%d\t", num);
    }

    system("pause");
    return 0;
}