/* 1) Ler um n�mero e ler um d�gito. Contar quantos d�gitos o n�mero possui. Exemplo:
� informado 5 como d�gito:
55 � possui 2 d�gitos cinco;
10 � possui nenhum d�gito cinco;
1550 � possui dois d�gitos cinco;
50050 � possui dois d�gitos cinco.
Repetir o programa enquanto informados valores positivos. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    int num, dig, contDig=0;
    char rep;

    do
    {
        setbuf(stdin,NULL);
        do
        {
            printf("Informe um n�mero: ");
            scanf("%d", &num);
            if(num>=0)
            {
                    do
                    {
                        printf("Informe um digito: ");
                        scanf("%d", &dig);
                        do
                        {contDig = contDig++;
                        dig = dig/10;
                        }while(dig!=0);
                        if(dig<0)
                        {
                            printf("Numero invalido! O numero deve ser positivo.\n");
                        }
                    }while(dig<0 || dig>10);
                printf("%d \n", &contDig);

            }
            else if(num<0)
            {
                printf("Numero invalido! O numero deve ser positivo.\n");
            }

        }while(num<=0);
        do
        {
            printf("Deseja Repetir? (S ou N)\n");
            setbuf(stdin, NULL);
            scanf("%c", &rep);
            rep=toupper(rep);
            if(rep != 'N' && rep != 'S')
            {
                printf("Caractere Invalido \n");
            }
        }while(rep != 'N' && rep != 'S');
     }while(rep == 'S');
    system("pause");
    return 0;
}

/*#include <stdio.h>
int main()
{
    int contaDigitos = 0, valor;
        scanf("%d", &valor);
        do
        {
        contaDigitos = contaDigitos + 1;
           valor = valor / 10;
       }
       while (valor != 0);
    printf("%d\n", contaDigitos);
    return 0;
}*/
