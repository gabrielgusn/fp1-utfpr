/* 4) Apresentar os n�meros primos entre dois valores, que representam os limites inferior e superior,
respectivamente, de um intervalo, informados pelo usu�rio. Apresent�-los com n n�meros por linha. n �
informado pelo usu�rio. Validar n para que seja maior que 0. Validar o limite inferior para seja maior que 1 e o
limite superior para que seja maior ou igual ao limite inferior. Implementar a repeti��o de programa. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main(void)
{
     int limInf, limSup, n, contDiv=0, inteiro=0, contPrint=0;
     char rep;
     do
     {
        do
        {
            printf("Informe o limite inferior: ");
            scanf("%d", &limInf);
            if(limInf<1)
            {
                printf("Limite Inferior Invalido \n");
            }

        }while(limInf<1);
        do
        {
            printf("Informe o limite superior: ");
            scanf("%d", &limSup);
            if(limInf>limSup)
            {
                printf("Limite Superior Invalido \n");
            }
        }while(limInf>limSup);
        do
        {
            printf("Informe quantos numeros deseja imprimir por linha: ");
            scanf("%d", &n);
            if(n<=0)
            {
                printf("Deve ser informado um valor maior que 0 \n");
            }
        }while(n<=0);
        for(int i=limInf; i<=limSup; i++)
        {
            contDiv=0;
            for(int num=1; num<=limSup; num++)
            {
               if(i%num==0)
                {
                    contDiv++;
                }
            }

                if(contDiv==2)
                {
                    printf("%5.d ", i);
                    contPrint++;
                }
                if(contPrint==n)
                {
                    contPrint=0;
                    printf("\n");
                }
            }
        do
        {
            printf("Deseja Repetir? (S ou N)\n");
            setbuf(stdin, NULL);
            scanf("%c", &rep);
            rep=toupper(rep);
            if(rep != 'N' && rep != 'S')
            {
                printf("Caractere Invalido \n");
            }
        }while(rep != 'N' && rep != 'S');
     }while(rep == 'S');


     return 0;
}
