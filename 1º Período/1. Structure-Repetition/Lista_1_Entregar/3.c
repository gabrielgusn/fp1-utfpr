/* 3) Completar e corrigir o c�digo a seguir para:
a) Ler a quantidade somente se a categoria � v�lida.
b) Garantir que a quantidade seja maior que 0.
c) Validar para que n�o seja realizada uma divis�o por zero no c�lculo da m�dia.
d) Permitir a leitura da categoria nas execu��es sucessivas do programa.
e) Garantir que a m�dia seja float */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main(void)
{
     char categoria;
     int quantidade, soma=0, total=0;
     float media;
         do
         {
             printf("Informe a categoria: ");
             setbuf(stdin,NULL);
             scanf("%c", &categoria);
             categoria=toupper(categoria);
             if(categoria == 'A' || categoria == 'B')
             {
                do
                {
                    printf("Informe a quantidade: ");
                    scanf("%d", &quantidade);
                    total++;
                }while(quantidade<=0);
                soma = soma + quantidade;
                media =((float)soma / total);
             }
             else
             {
                 printf("Categoria Invalida \n");
             }
         }while(categoria == 'A' || categoria== 'B');

     printf("A media dos produtos eh %.2f", media);
     return 0;
}
