/* 2) Uma empresa deseja calcular a depreciação de seus bens. Para tanto, desenvolver um programa que
obtenha a taxa de depreciação anual para os bens, o valor do bem a ser depreciado e o período em anos.
Valor depreciado = valor do bem * (taxa de depreciação / 100)
Valor do bem depreciado = valor do bem – valor depreciado */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    float taxaDepr, valBem, valDepr, deprAcum;
    int ano, contAno=0;

    printf("Informe o valor do bem a ser depreciado : R$ ");
    scanf("%f", &valBem);
    printf("Informe o periodo da depreciacao (em anos): ");
    scanf("%d", &ano);
    printf("Informe a taxa de depreciacao(em %%): ");
    scanf("%f", &taxaDepr);

    printf("Ano\t Valor do Bem\t Depreciacao \t Valor Depreciado\n");
    printf("=========================================================\n");

    for(int i=0; i<ano; i++)
    {
        valDepr = valBem*(taxaDepr/100);
        contAno++;
        printf("%d \t %10.2f \t%10.2f\t\t%10.2f\n", contAno, valBem, valDepr, valBem-valDepr);
        deprAcum= deprAcum+valDepr;
        valBem = valBem-valDepr;
        valDepr = valBem*(taxaDepr/100);

    }
    printf("=========================================================\n");
    printf("Depreciacao acumulada : R$ %.2f\n", deprAcum);
    system("pause");
    return 0;
}
