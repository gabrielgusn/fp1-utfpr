#include <stdio.h>
int main(void)
{
    /*declara��o de vari�veis*/
    int num1, num2, soma;

    //Entrada de Dados
    printf("Informe um numero: ");
    scanf("%d", &num1);
    printf("Informe outro numero: ");
    scanf("%d", &num2);

    //Processamento de Dados
    soma = num1 + num2;

    //Sa�da de Dados
    printf("Resultado: %d", soma);

    return 0;

}
